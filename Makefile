PY = python3
# 已过时，用于早期调试使用
run:
	@$(PY) ./src/main.py

clean:
	@rm -rf *.gv *.pdf ./src/*.gv ./src/*.pdf ./src/*.svg
	@rm -rf ./tmp/* ./web/*