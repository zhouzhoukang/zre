# -*- coding: utf-8 -*-
# FileName:     TestBNF.py
# Time:         23/8/14 014 下午 3:28
# Author:       Zhou Hang
# Description:  BNF 读取器测试
import unittest

from src.core.Components.BNF import BNF, FFBNF


class TestBNF(unittest.TestCase):
    def test_01(self):
        bnf = BNF('../../input/calc_2.txt')
        bnf.show()

    def test_02(self):
        bnf = FFBNF('../../input/calc_2.txt')
        bnf.calc_first()
        bnf.calc_follow()
        bnf.show()
        bnf.show_ff()
