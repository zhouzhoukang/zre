# -*- coding: utf-8 -*-
# FileName:     TestSLR.py
# Time:         23/8/14 014 下午 4:12
# Author:       Zhou Hang
# Description:  I don't want to write.
import unittest
from src.core.SLR import SLRBNF


class TestOperatorFirst(unittest.TestCase):
    def test_01(self):
        ebnf = SLRBNF('../../input/calc_2.txt')
        ebnf.show_ff()
        ebnf.show()
        ebnf.show_projects()
        ebnf.show_dfa('../../output/tmp/calc_lr0.gv')
        ebnf.LR0('(i+i+i)*i#')

    def test_02(self):
        ebnf = SLRBNF('../../input/G6.1.txt')
        ebnf.LR0('acd#')

    def test_03(self):
        ebnf = SLRBNF('../../input/not_slr.txt')
        ebnf.LR0('L=R#')

