# -*- coding: utf-8 -*-
# FileName:     TestLL1.py
# Time:         23/8/14 014 下午 3:57
# Author:       Zhou Hang
# Description:  LL1 文法测试
import unittest

from src.core.UpDown import LL1


class TestBNF(unittest.TestCase):
    def test_01(self):
        bnf = LL1('../../input/calc.txt')
        bnf.show()
        bnf.recursive_descent_parsing('a+a*a#')

    def test_02(self):
        bnf = LL1('../../input/vscode-snippet.txt')
        bnf.show()
        bnf.recursive_descent_parsing('1')
