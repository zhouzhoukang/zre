# -*- coding: utf-8 -*-
# FileName:     TestErrorHandling.py
# Time:         23/8/13 013 下午 2:12
# Author:       Zhou Hang
# Description:  错误处理模块测试
import unittest

from src.tools.zerror import BaseError, ZAssert


class TestErrorHandling(unittest.TestCase):
    def test_exception_01(self):
        ZAssert(False, "123")
