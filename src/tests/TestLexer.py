# -*- coding: utf-8 -*-
# FileName:     TestLexer.py
# Time:         23/8/13 013 下午 1:51
# Author:       Zhou Hang
# Description:  I don't want to write.
import unittest

from src.core.Zlex import ZLexer


class TestLexer(unittest.TestCase):
    def test_split_lex_line(self):
        lex = ZLexer()
        token_name, reg = lex.split_lex_line("Continue : 'continue';\n")
        self.assertEqual(token_name, "Continue")
        self.assertEqual(reg, "'continue'")

        token_name, reg = lex.split_lex_line("Auto : 'auto';\n")
        self.assertEqual(token_name, "Auto")
        self.assertEqual(reg, "'auto'")

        token_name, reg = lex.split_lex_line("DecimalConstant : [1-9][0-9]* ;\n")
        self.assertEqual(token_name, "DecimalConstant")
        self.assertEqual(reg, "[1-9][0-9]*")

    def test_compiler_regex(self):
        lex = ZLexer()
        reg = lex.compile_regex("'auto'")
        res = reg.match("auto int a = 1;")
        self.assertEqual(res.span(), (0, 4))

        reg = lex.compile_regex("'||'")
        res = reg.match("|| a == b")
        self.assertEqual(res.span(), (0, 2))

        reg = lex.compile_regex("'+'")
        res = reg.match("+ i")
        self.assertEqual(res.span(), (0, 1))

        reg = lex.compile_regex("[1-9][0-9]*")
        res = reg.match("16;")
        self.assertEqual(res.span(), (0, 2))
        self.assertEqual(res.group(), "16")

        reg = lex.compile_regex("0[xX][0-9a-fA-F]+")
        res = reg.match("0x14FF;;;")
        self.assertEqual(res.span(), (0, 6))
        self.assertEqual(res.group(), "0x14FF")

    def test_add_lex_file(self):
        lex = ZLexer()
        lex.add_lex_file('../../input/lexer/lex0.txt')
        res = lex.tokens[0].match('if (a == b) {')
        self.assertEqual(res.span(), (0, 2))
        self.assertEqual(res.group(), "if")

