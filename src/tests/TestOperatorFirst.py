# -*- coding: utf-8 -*-
# FileName:     TestOperatorFirst.py
# Time:         23/8/14 014 下午 4:10
# Author:       Zhou Hang
# Description:  算符优先文法测试
import unittest
from src.core.OperatorFirst import OperatorFirst


class TestOperatorFirst(unittest.TestCase):
    def test_01(self):
        bnf = OperatorFirst('../../input/calc_2.txt')
        bnf.show_tables()
        bnf.shift_reduce('i*(i+i)' + '#')
