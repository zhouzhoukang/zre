# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ZCompiler.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Choose_widget(object):
    def setupUi(self, Choose_widget):
        Choose_widget.setObjectName("Choose_widget")
        Choose_widget.resize(315, 217)
        font = QtGui.QFont()
        font.setFamily("等线")
        font.setPointSize(12)
        Choose_widget.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../resource/pic/main_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Choose_widget.setWindowIcon(icon)
        self.gridLayout = QtWidgets.QGridLayout(Choose_widget)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 36, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(12, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.open_lex_btn = QtWidgets.QPushButton(Choose_widget)
        self.open_lex_btn.setMinimumSize(QtCore.QSize(120, 100))
        self.open_lex_btn.setObjectName("open_lex_btn")
        self.horizontalLayout.addWidget(self.open_lex_btn)
        self.open_grammar_btn = QtWidgets.QPushButton(Choose_widget)
        self.open_grammar_btn.setMinimumSize(QtCore.QSize(120, 100))
        self.open_grammar_btn.setObjectName("open_grammar_btn")
        self.horizontalLayout.addWidget(self.open_grammar_btn)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 1, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(12, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 1, 2, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 37, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 2, 1, 1, 1)

        self.retranslateUi(Choose_widget)
        QtCore.QMetaObject.connectSlotsByName(Choose_widget)

    def retranslateUi(self, Choose_widget):
        _translate = QtCore.QCoreApplication.translate
        Choose_widget.setWindowTitle(_translate("Choose_widget", "Choose"))
        self.open_lex_btn.setText(_translate("Choose_widget", "词法分析器"))
        self.open_grammar_btn.setText(_translate("Choose_widget", "语法分析器"))
