# -*- coding: utf-8 -*-
# FileName:     OperatorFirst.py
# time:         22/10/8 008 下午 5:39
# Author:       Zhou Hang
# Description:  自底向上分析 - 算符优先文法,包括求取firstvt/lastvt/算符优先表/算符优先文法
from src.core.Components.BNF import *
from prettytable import PrettyTable
from src.tools.zlog import Zlog
from src.tools.const import Priority, Const


# 不需要处理空的情况
class VT_T_Array:
    """
    使用字典的二维表格，用于封装lastvt firstvt 算符优先表
    由于也需要用在优先分析表中,此二维表格完全应该屏蔽列标和行标的区别,由于早期思维,某些变量名没有修改
    每个元素只有五种可能的值 False/True False/Priority.LOW/Priority.EQUAL/Priority.HIGH
    """
    def __init__(self, col_mark, row_mark, name):
        self.col_mark = col_mark
        self.row_mark = row_mark
        self.name = name
        self.array = {}
        for i in range(len(self.col_mark)):
            temp = []
            for j in range(len(self.row_mark)):
                temp.append(False)
            self.array[col_mark[i]] = temp

    def get_true(self, col_name):
        """生成获得给定名称的集合元素"""
        for idx, i in enumerate(self.array[col_name]):
            if i:
                yield self.row_mark[idx]

    def _get_j(self, nt_name: str, t_name: str) -> int:
        """
        确保 set_item 和 get_item 一定正确
        :param nt_name: 行名
        :param t_name: 列名
        """
        try:
            idx = self.row_mark.index(t_name)
            _ = self.array[nt_name]
            return idx
        except ValueError or KeyError:
            Zlog("错误的符值")
            exit(1)

    def get_item(self, nt_name: str, t_name: str):
        idx = self._get_j(nt_name, t_name)
        return self.array[nt_name][idx]

    def set_item(self, nt_name: str, t_name: str, value):
        idx = self._get_j(nt_name, t_name)
        self.array[nt_name][idx] = value

    def get_priority(self, col_name: str, row_name: str):
        """为了改个名字,增强可读性,仅仅为算符优先分析表设计"""
        priority = self.get_item(col_name, row_name)
        if not priority:
            Zlog(f'不存在从 {col_name} 到 {row_name} 的优先级！')
            exit(1)
        return priority

    def show(self, dim: int = 2):
        """
        :param dim: 表示表格元素值的可取值个数
        :return:
        """
        print(self.name + ":")
        table = PrettyTable([' '] + [str(i) for i in self.row_mark])
        if dim == 2:
            for nt in self.col_mark:
                table.add_row([nt] + ['✓' if i else ' ' for i in self.array[nt]])
        elif dim == 3:
            equal = Zlog('≖', color='YELLOW', if_print=False, show_info=False).get_str()
            low = Zlog('⋖', color='RED', if_print=False, show_info=False).get_str()
            high = Zlog('⋗', color='GREEN', if_print=False, show_info=False).get_str()
            for nt in self.col_mark:
                # 这行代码简直绝了, 牛逼
                table.add_row([nt] + [equal if i == Priority.EQUAL else low if i == Priority.LOW
                              else high if i == Priority.HIGH else ' ' for i in self.array[nt]])
        print(table)


class OperatorFirst(BNF):
    def __init__(self, bnffile_path):
        super(OperatorFirst, self).__init__(bnffile_path)
        # ================ 为qt界面使用 ==================
        self.a_action = []
        self.a_now_input_str = []
        self.a_analyze_stack = []
        # ==============================================
        self.firstvt = VT_T_Array(self.non_terminals, self.terminals, 'firstvt')
        self.lastvt = VT_T_Array(self.non_terminals, self.terminals, 'lastvt')
        self._calc_firstvt_lastvt()
        stack_end = self.bottom.name
        self.priority_table = VT_T_Array(self.terminals + [stack_end], self.terminals + [stack_end], 'priority_table')
        self._calc_priority_table()

    def analyze_length(self):
        """返回分析的次数，为qt界面使用"""
        return len(self.a_analyze_stack)

    def _calc_firstvt_lastvt(self):
        firstvt_stack = []  # 记录所有新的添加
        lastvt_stack = []
        # 初始化firstvt
        for production in self.productions:
            P = production.get_left_name()
            for right in production.gen_right():
                # 由于算符优先文法的特性，两个非终结符不会连在一起，这部分代码比较容易理解
                if right[0].is_terminal:
                    a = right[0].name
                    self.firstvt.set_item(P, a, True)
                    firstvt_stack.append((P, a))
                elif len(right) > 1 and right[1].is_terminal:
                    a = right[1].name
                    self.firstvt.set_item(P, a, True)
                    firstvt_stack.append((P, a))
                if right[-1].is_terminal:
                    a = right[-1].name
                    self.lastvt.set_item(P, a, True)
                    lastvt_stack.append((P, a))
                elif len(right) > 1 and right[-2].is_terminal:
                    a = right[-2].name
                    self.lastvt.set_item(P, a, True)
                    lastvt_stack.append((P, a))
        # 此处代码虽然冗余,但是由于不存在扩展性的问题,就不改了
        while firstvt_stack:
            Q, a = firstvt_stack.pop()
            for production in self.productions:
                P = production.get_left_name()
                for right in production.gen_right():
                    if right[0].name == Q and not self.firstvt.get_item(P, a):
                        self.firstvt.set_item(P, a, True)
                        firstvt_stack.append((P, a))
        while lastvt_stack:
            Q, a = lastvt_stack.pop()
            for production in self.productions:
                P = production.get_left_name()
                for right in production.gen_right():
                    if right[-1].name == Q and not self.lastvt.get_item(P, a):
                        self.lastvt.set_item(P, a, True)
                        lastvt_stack.append((P, a))

    def _test_and_set(self, t1, t2, value):
        """
        在这种情况下每个表元素只有四种可能的值并且不存在扩展性问题
        :param t1: 行名
        :param t2: 列名
        :param value: 将要设置的值
        :return:
        """
        if self.priority_table.get_item(t1, t2) not in [value, False]:
            Zlog('不是算符优先文法!')
            exit(1)
        self.priority_table.set_item(t1, t2, value=value)

    def _calc_priority_table(self):
        for production in self.productions:
            # 对于T NT T 的情况额外进行考虑 符号优先级是
            for first, second, third in production.gen_token_sequence(length=3):  # 这个生成器写得非常好
                # TODO:此处每次设置表格项之前都要判断原本的值和将要设置的值是否一致,导致冗余(已解决)
                if first.is_terminal and third.is_terminal and not second.is_terminal:
                    self._test_and_set(first.name, third.name, Priority.EQUAL)
            for first, second in production.gen_token_sequence():  # 使用生成器获取长度2的序列
                if first.is_terminal and second.is_terminal:  # 优先级相同
                    self._test_and_set(first.name, second.name, Priority.EQUAL)
                elif not first.is_terminal and second.is_terminal:  # 优先级左小 LOW
                    # last(first)中的每个符号,都比second优先级高
                    for higher in self.lastvt.get_true(first.name):
                        self._test_and_set(higher, second.name, Priority.HIGH)
                elif first.is_terminal and not second.is_terminal:  # 优先级左大 HIGH
                    # first比firstvt(second)中的每个符号优先级高
                    for higher in self.firstvt.get_true(second.name):
                        self._test_and_set(first.name, higher, Priority.LOW)
                elif not first.is_terminal and not second.is_terminal:  # 算符优先文法不存在两个终结符连在一起的情况
                    Zlog("不是算符优先文法")
                    exit(1)
        # 单独考虑栈底符的情况, 虚拟一个 # S(开始符) # 的产生式
        start_name, end_name = self.start.name, self.bottom.name
        # 栈底符自己和自己优先级相同
        self._test_and_set(end_name, end_name, Priority.EQUAL)
        # 栈底符低于firstvt(开始符)
        for higher in self.firstvt.get_true(start_name):
            self._test_and_set(end_name, higher, Priority.LOW)
        # 栈底符高于lastvt(开始符)
        for higher in self.lastvt.get_true(start_name):
            self._test_and_set(higher, end_name, Priority.HIGH)

    def priority_table_get_str(self, t1, t2):
        """查表根据优先表的优先关系返回相应符号，为qt界面使用，这个函数名起得非常差劲"""
        priority = self.priority_table.get_item(t1, t2)
        return '≖' if priority == Priority.EQUAL else '⋖' if priority == Priority.LOW \
            else '⋗' if priority == Priority.HIGH else ' '

    def show_tables(self):
        """显示firstvt/lastvt/优先表"""
        self.firstvt.show(2)
        self.lastvt.show(2)
        self.priority_table.show(3)  # 测试

    def _match_production(self, to_match):
        """
        从所有产生式中寻找与需要规约的串匹配的产生式的左部
        匹配原则：对应位置的token类型必须一一对应，其中如果是终结符，其值也必须一一对应，若是非终结符就不需要
        """
        for left, right in self._for_each_production():
            if len(right) == len(to_match):  # 只有长度相同才有可能match
                if_match = True
                for _idx in range(len(to_match)):
                    t1, t2 = right[_idx], to_match[_idx]
                    if t1.is_terminal and not t2.is_terminal:
                        if_match = False
                    elif not t1.is_terminal and t2.is_terminal:
                        if_match = False
                    elif t1.is_terminal and t2.is_terminal:
                        if t1 is not t2:
                            if_match = False  # 只要发现一个不对应的就匹配失败
                    if not if_match:
                        break
                if if_match:
                    return left
        Zlog(f'{to_match}未能匹配到合适的产生式!')
        exit(1)

    def shift_reduce(self, input_str: str = ''):
        """
        解决两个问题
        - 什么时候需要shift
        - 什么时候需要reduce
        暂时没有解决空格的问题
        这里涉及最左素短语的原理,可以看这个来理解
        https://www.bilibili.com/video/BV12741147J3?p=101&vd_source=79455df0a6c48b0822d32bafe7e8f7c6
        """
        def _find_top_t_id() -> int:
            """
            如果栈顶是终结符就指向栈顶，否则指向栈顶下一个元素
            :return:
            """
            if analysis_stack[-1].is_terminal:
                ret = len(analysis_stack) - 1
            else:
                ret = len(analysis_stack) - 2
            return ret

        def _find_next_t_id(cur) -> int:
            """
            只寻找终结符遍历
            :param cur: 在栈中寻找cur下标之后的一个终结符的下标
            :return:
            """
            try:
                if analysis_stack[cur - 1].is_terminal:  # TODO: 这里有没有可能数组越界
                    return cur - 1
                else:
                    return cur - 2
            except IndexError:
                return 0

        def _push_log(info, to_push, to_reduce=None, _input_str=None):
            """
            用于记录shift和reduce信息 TODO: 不知道能不能像C一样可以通过宏来开启或关闭某个函数
            :return:
            """
            stack_show = Zlog(str(analysis_stack), color='CYAN', if_print=False, show_info=False).get_str()
            self.a_analyze_stack.append(str(analysis_stack))
            if info == Const.SHIFT:
                self.a_now_input_str.append(_input_str)
                _input_str = Zlog(_input_str, color='YELLOW', if_print=False, show_info=False).get_str()
                shift_show = str(to_push)
                print('shift {0: ^18} | analysis_stack = {1: ^35} | {2}'.format(shift_show, stack_show, _input_str))
                self.a_action.append(f'移进  {shift_show}')
            elif info == Const.REDUCE:
                reduce_show = f'{to_reduce} → {to_push}'
                print('reduce {0: ^17} | analysis_stack = {1: ^35}'.format(reduce_show, stack_show))
                self.a_action.append(f'规约 {reduce_show}')
                self.a_now_input_str.append(' ')

        bottom_name = self.bottom.name
        i = 0  # 索引input_str，纸带读写头
        top_id, top_t_id = 0, 0  # 指向真正的栈顶 / 指向栈顶终结符,一定是栈顶或栈顶底下一个元素
        analysis_stack = [self.bottom]
        # TODO: 此处由于算符优先文法的特性,最后剩的不一定是开始符,也可能是开始符的某个单独的产生式,所以这里的while条件看似并不是符合的
        # 一旦栈中还剩一个开始符就结束
        while True:
            if len(analysis_stack) == 2 and not analysis_stack[1].is_terminal and input_str[i] == bottom_name:
                break
            top, top_t = analysis_stack[top_id], analysis_stack[top_t_id]  # 这里的top好像没啥用]
            # 如果输入头读到栈底符号了，就不需要这段代码了，直接规约就行
            if input_str[i] != bottom_name:
                matched_terminal = self._find_matched_terminal_token(input_str[i:])  # 注意这里i需要在合适的时候更新
                # shift条件: 当栈顶终结符优先级低于或等于下一个读入的token
                priority = self.priority_table.get_priority(top_t.name, matched_terminal.name)
                if priority == Priority.LOW or priority == Priority.EQUAL:  # 需要shift压栈,等待日后先行规约
                    analysis_stack.append(matched_terminal)
                    i += len(matched_terminal.name)
                    _push_log(Const.SHIFT, matched_terminal, _input_str=input_str[i:])
                    top_id += 1
                    top_t_id = _find_top_t_id()
                    continue  # 继续移入操作
            # reduce条件: assert priority == Priority.HIGH 或者输入串读到底了

            # 抠出最左素短语进行规约,从栈顶开始向下遍历栈
            _idx = _find_next_t_id(top_t_id)  # 使用该变量来遍历栈
            prime_phrase_start, prime_phrase_end = top_t_id, top_id
            while True:
                p_stack = analysis_stack[_idx]
                p_prev_stack = analysis_stack[prime_phrase_start]
                priority2 = self.priority_table.get_priority(p_stack.name, p_prev_stack.name)
                if priority2 == Priority.LOW:
                    prime_phrase_start = _idx + 1
                    break
                assert priority2 != Priority.HIGH  # 这里不可能出现这种情况
                prime_phrase_start = _idx  # 记录上一个_idx值
                _idx = _find_next_t_id(_idx)

            # 获得了一个 (prime_phrase_start, prime_phrase_end) 最左素短语,接下来将其规约为一个非终结符
            # 方便起见把本来想把需要规约的统统取出来放到单独空间中处理,并出栈
            to_match = analysis_stack[prime_phrase_start: prime_phrase_end + 1]
            del analysis_stack[prime_phrase_start: prime_phrase_end + 1]
            top_id = len(analysis_stack) - 1
            top_t_id = _find_top_t_id()
            left = self._match_production(to_match)  # 获得规约之后的非终结符
            analysis_stack.append(left)
            _push_log(Const.REDUCE, left.name, to_match)
            top_id += 1
        Zlog('算符优先文法匹配成功！', color='GREEN', show_info=False)


def main():
    bnf = OperatorFirst('../../input/calc_2.txt')
    bnf.show_tables()
    bnf.shift_reduce('i*(i+i)' + '#')


if __name__ == "__main__":
    main()
