# -*- coding: utf-8 -*-
# FileName:     Zlex.py
# time:         22/11/8 008 上午 8:06
# Author:       Zhou Hang
# Description:  词法分析程序，输入一个词法文件，通过这个词法文件去识别另外一个源程序
from pathlib import Path
import re
from typing import List
from prettytable import PrettyTable
from src.tools.zerror import ZAssert, ZPass


class ZLexer:
    class Token:
        def __init__(self, name: str, regex: re.Pattern) -> None:
            self.name = name
            self.regex = regex

        def __str__(self):
            return f'{self.name} : {self.regex.pattern}'

        def match(self, _str: str):
            return self.regex.match(_str)

    class Result:
        def __init__(self, token_name: str, matched: str, line_no: int, col_no: int):
            """
            :param token_name:
            :param matched: 匹配成功的字符串
            :param line_no: 行号
            :param col_no: 列号
            """
            self.token_name = token_name
            self.matched = matched
            self.line_no = line_no
            self.col_no = col_no

        def __str__(self):
            return f"{self.token_name}\t\t{self.matched}\t{self.line_no}\t{self.col_no}"

    class Error:
        def __init__(self, line_no, col_no):
            self.line_no = line_no
            self.col_no = col_no

        def __str__(self):
            return f'第 {self.line_no} 行，第 {self.col_no} 列无法识别'

    def __init__(self):
        self.res_table = None
        self.err_table = None
        self.lex_file = None  # lex 文件
        self.src_file = None  # 目标文件

        self.tokens: List[ZLexer.Token] = []
        self.errors: List[ZLexer.Error] = []  # 所有错误信息
        self.results: List[ZLexer.Result] = []  # 最终匹配结果

    @staticmethod
    def compile_regex(_regex: str) -> re.Pattern:
        """
        分为两种正则表达式，一种是原文匹配，用单引号引出来，一种是正则匹配
        :param _regex: 正则表达式字符串
        :return: re.compiler 后的结果
        """
        if _regex.startswith('\''):
            _regex = eval(_regex)  # 去掉单引号
            if len(_regex) == 1:  # re 元符号处理
                return re.compile(r'[\\{}]'.format(_regex))
            try:
                if _regex in ['||', '|=', '^=', "..."]:  # re 元符号处理，TODO
                    raise re.error('OrOr')
                return re.compile(r'{}'.format(_regex))
            except re.error:  # 特殊字符需要转义，这里一定有更好的办法
                regex_list = list(_regex)
                # 在每个字符前插入转义字符
                regex_list.insert(1, '\\')
                regex_list.insert(0, '\\')
                _regex = ''.join(regex_list)
                return re.compile(r'{}'.format(_regex))
        return re.compile(_regex)

    def split_lex_line(self, line: str) -> (str, str):
        """
        将一行 lex 通过遇到的第一个冒号分割成 token name 正则表达式
        :param line: 形如 “DecimalConstant : [1-9][0-9]* ;” 的行
        :return: token 名称和其对应的正则表达式
        """
        line = line.split(':', 1)
        ZAssert(len(line) == 2, f"{self.lex_file} 语法错误！")
        return line[0].strip(), line[1].strip('\n').strip(';').strip()

    def add_lex_file(self, lex_file_path: str) -> bool:
        self.lex_file = Path(lex_file_path)
        ZAssert(self.lex_file.exists(), "lexer 文件不存在")
        return self.parse_lex_file()

    def parse_lex_file(self) -> bool:
        """
        处理 lex 文件, 不支持多行处理，需要把所有正则表达式写在同一行，用分号结尾
        :return: 是否处理成功, 目前如果处理失败会直接 exit
        """
        with self.lex_file.open(encoding='utf-8') as lex:
            for line in lex:
                if line.startswith('//') or line == '\n':  # 注释行和空行直接跳过
                    continue
                # 提取出 : 前后的字符串
                token_name, regex = self.split_lex_line(line)
                ZAssert(len(token_name) != 0, f"{self.lex_file} 语法错误！")
                token = self.Token(token_name, self.compile_regex(regex))
                self.tokens.append(token)
        return True

    def add_src_file(self, src_file_path):
        self.src_file = Path(src_file_path)
        ZAssert(self.src_file.exists(), "src 文件不存在")

    def run(self):
        with self.src_file.open(encoding='utf-8') as src:
            for line_no, line in enumerate(src):  # 按行匹配
                self.lex(line, line_no + 1)
        ZPass(msg='词法分析完成')

    def lex(self, line, line_no):
        """
        根据源文件某一行进行词法分析
        :return:
        """
        i = 0  # 列号
        while i < len(line):
            has_matched = False  # 是否匹配到
            # 遍历所有 token，对从i开始的子串进行匹配
            for token in self.tokens:
                match_result = token.match(line[i:])
                if match_result is not None:
                    result = match_result.span()
                    matched_str = match_result.group()
                    # matched_str = line[i:i + result[1]]
                    if matched_str not in [' ', '\n']:
                        self.results.append(
                            ZLexer.Result(token.name, matched_str, line_no, i)
                        )
                    i += result[1]
                    has_matched = True
                if has_matched:
                    break
            if not has_matched:
                self.errors.append(ZLexer.Error(line_no, i))
                return  # 忽略错误行，继续向下处理

    def gen_beautiful_table(self):
        self.err_table = PrettyTable(['Error Message'])
        for error in self.errors:
            self.err_table.add_row([str(error)])
        self.res_table = PrettyTable(['Token', "Matched", "Line", "Column"])
        for result in self.results:
            self.res_table.add_row([result.token_name, result.matched, result.line_no, result.col_no])
    
    def show(self):
        self.gen_beautiful_table()
        print(self.res_table)
        print(self.err_table)


def main():
    zlex = ZLexer()
    zlex.add_lex_file('../../input/lexer/lex1.txt')
    zlex.add_src_file('../../input/lexer/src1.txt')
    zlex.run()
    zlex.show()


if __name__ == "__main__":
    main()
