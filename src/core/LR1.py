# -*- coding: utf-8 -*-
# FileName:     LR1.py.py
# time:         22/11/15 015 下午 11:43
# Author:       Zhou Hang
# Description:  TODO:在SLR基础上，通过重写DFA等方法生成LR分析器
from src.core.SLR import *
from src.tools.misc import lists_eq


class LR1_Project(Project):
    """
    为LR1添加一个前看符号的项目类
    这里项目编号一致不一定前看符号一致！所以引入新的编号？TODO:暂时不知道怎么处理
    """

    def __init__(self, lr1_id, _id, left, right, dot_pos):
        super(LR1_Project, self).__init__(_id, left, right, dot_pos)
        self.lr1_id = lr1_id  # 通过_id和lookahead来判断是否是同一个项目
        # 这里前看符号并不是只要一个就行的，在生成DFA的时候有可能出现合并的情况，这边考虑把前看符号也封装了
        self.look_ahead = []  # 前看符号, 用于LR(1)算法使用，前看符号一定是终结符或#

    def add_lookahead(self, token_name):
        """去重增加一个前看符号名 TODO:要不要去重呢"""
        if token_name in self.look_ahead:
            return
        self.look_ahead.append(token_name)

    def add_lookaheads(self, token_names):
        """去重增加多个前看符号"""
        assert isinstance(token_names, list)
        for token_name in token_names:
            self.add_lookahead(token_name)

    def __eq__(self, other):
        """
        凭借id号以及前看符号来确定两者是否相同
        项目id一致不一定前看符号一致
        :param other:
        :return:
        """
        assert isinstance(other, LR1_Project)
        # if self.id == other.id and self.look_ahead == other.look_ahead:
        if self.id == other.id:
            assert self.left.name == other.left.name
            if lists_eq(self.look_ahead, other.look_ahead):
                return True
        return False

    def __repr__(self):
        super_str = super(LR1_Project, self).__repr__()
        return f'{super_str} {self.look_ahead}'


class LR1_ProjectDFANode(ProjectDFANode):
    def __init__(self, _id):
        super(LR1_ProjectDFANode, self).__init__(_id)

    def _get_id_set(self):
        """
        为eq方法做准备，得到一个id的集合
        通过新的lr1_id判定一致
        :return:
        """
        ret = set()
        for proj in self.projects:
            ret.add(proj.lr1_id)
        return ret

    def __eq__(self, other):
        assert isinstance(other, LR1_ProjectDFANode)
        if len(self) != len(other):
            return False
        return self._get_id_set() == other._get_id_set()


class LR1BNF(SLRBNF):
    def __init__(self, bnffile_path):
        super(LR1BNF, self).__init__(bnffile_path)

    def _gen_dfa(self):
        pass


def main():
    lr1_bnf = LR1BNF('../../input/test_LR1.txt')
    lr1_bnf.projects[1]
    lr1_bnf.show()


if __name__ == "__main__":
    main()
