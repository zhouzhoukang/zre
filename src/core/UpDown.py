# -*- coding: utf-8 -*-
# FileName:     UpDown.py
# time:         22/10/4 004 下午 10:19
# Author:       Zhou Hang
# Description:  递归下降算法，包括First/Follow/预测分析表/PDA/LL(1)
from src.core.Components.BNF import *


class PredictionTable:
    """
    使用二维列表存储预测分析表,每一个表项要么是ERROR要么是一个包括Production对象和其right下标的二元组
    TODO:完全可以使用字典甚至字典的字典来处理二维数组
    """
    def __init__(self, non_terminals: List[str], terminals: List[str], bottom_name: str = '#'):
        self.non_terminals: List[str] = non_terminals
        self.terminals: List[str] = terminals
        self.terminals.append(bottom_name)
        self.table: List[List[Optional[tuple[Production, int], int]]] = []
        # 将二维表初始化为空
        for _ in range(len(self.non_terminals)):
            line = []
            for __ in range(len(self.terminals)):
                line.append(-1)  # -1 表示 Error
            self.table.append(line)

    def _get_ij(self, nt_name: str, t_name: str) -> tuple[int, int]:
        """通过名称获得二维表两个下标"""
        i, j = 0, 0
        try:
            i = self.non_terminals.index(nt_name)
            j = self.terminals.index(t_name)
        except ValueError:  # 输入了错误的符号名导致index错误
            Zlog('不存在的符号名')
            exit(1)
        return i, j

    def get_item(self, nt_name: str, t_name: str) -> Optional[tuple[Production, int]]:
        i, j = self._get_ij(nt_name, t_name)
        return self.table[i][j]

    def set_item(self, nt_name: str, t_name: str, production: Production, right_idx: int) -> None:
        i, j = self._get_ij(nt_name, t_name)
        if self.table[i][j] != -1:
            Zlog(f'输入的不是LL(1)文法 冲突:M({nt_name},{t_name})', color='YELLOW')
            exit(-1)
        self.table[i][j] = (production, right_idx)

    def show(self):
        table = PrettyTable([' '] + self.terminals)
        for non_terminal in self.non_terminals:
            row_str = []
            for terminal in self.terminals:
                item: Optional[tuple[Production, int]] = self.get_item(non_terminal, terminal)
                if item == -1:
                    row_str.append(Zlog('Error', if_print=False, show_info=False).get_str())
                else:
                    assert isinstance(item[0], Production)
                    to_show = item[0].get_one_str(item[1])
                    row_str.append(Zlog(to_show, color='GREEN', if_print=False, show_info=False).get_str())
            table.add_row([non_terminal] + row_str)
        print(table)


class LL1(FFBNF):
    """自顶向下分析之LL(1)分析法,包括求取first首符集/follow随符集/预测分析表/递归下降"""
    def __init__(self, bnffile_path: str):
        super(LL1, self).__init__(bnffile_path)
        self.a_action = []
        self.a_now_input_str = []
        self.a_pda_stack = []
        self.calc_first()
        self.calc_follow()
        self._gen_prediction_table()

    def _gen_prediction_table(self):
        epsilon: str = ''
        if self.epsilon is not None:
            epsilon = self.epsilon.name
        else:
            Zlog('不存在epsilon')
            exit(-1)
        # 如果是非左递归文法，大部分情况下是不是都会存在epsilon？如果不存在epsilon是不是可以认定这不是LL1文法？
        self.prediction_table: PredictionTable = PredictionTable(self.non_terminals, self.terminals)
        for production in self.productions:
            left = production.get_left_name()
            for idx, right in enumerate(production.gen_right()):
                token, use_follow = right[0], False
                if token.is_terminal and token.name != epsilon:  # 产生式第一个就是非空终结符
                    self.prediction_table.set_item(left, token.name, production, idx)
                elif token.name == epsilon:  # 产生式第一个就是空，使用left的随符集
                    use_follow = True
                else:  # 产生式第一个是非终结符，遍历left首符集
                    for first in self.first[token.name]:
                        if first.name == epsilon:  # 该非终结符的首符集包含空，使用left随符集
                            use_follow = True
                        else:
                            self.prediction_table.set_item(left, first.name, production, idx)
                if use_follow:
                    for follow in self.follow[left]:  # 永远使用的是left的随符集
                        self.prediction_table.set_item(left, follow.name, production, idx)

    def analyze_length(self):
        """返回分析的次数，为qt界面使用"""
        return len(self.a_pda_stack)

    def show(self):
        super().show()
        super(LL1, self).show_ff()
        self.prediction_table.show()

    def recursive_descent_parsing(self, input_str: str = ''):
        """
        – 初始化时，栈中仅包含开始符号S (和栈终结符号)
        – 1.如果栈顶元素是终结符号，那么进行匹配
        – 2.如果栈顶元素是非终结符号
            3.使用预测分析表来选择 正确产生式
            4.在栈顶用 产生式右部 替换 产生式左部
        :param input_str:
        :return:
        """
        self.a_pda_stack = []
        self.a_now_input_str = []
        self.a_action = []

        def print_status(action, action_param=''):
            """将递归下降分析过程中的状态统统记录下来，在qt中调用"""
            self.a_pda_stack.append(f'{str(pda_stack): ^30}')
            self.a_now_input_str.append(f'{input_str[i:]: ^15}')
            self.a_action.append(f'{action_param}{action}')
            print(f'{str(pda_stack): ^30} {input_str[i:]: ^15} {action_param}{action}')

        i = 0  # 索引input_str，纸带读写头
        pda_stack = [self.bottom, self.start]
        top = pda_stack.pop()
        while top:
            if top.is_terminal:  # 情况1，直接匹配
                top_name = top.name
                for ch in top_name:
                    if ch != input_str[i]:
                        Zlog(f'{input_str}[i={i}]不符合文法！')
                        exit(1)
                    i += 1
                print_status('直接匹配')
            else:  # 情况2，栈顶是非终结符
                # 匹配当前输入流中的第一个token
                matched_terminal = self._find_matched_terminal(input_str[i:])
                # i += len(matched_terminal)   # 这里还没匹配掉呢，所以不能这么写
                # 根据预测分析表，选择合适的产生式, 记得这里的prefer_production是个元组或-1
                prefer_production: Optional[tuple[Production, int]] = self.prediction_table.get_item(top.name, matched_terminal)
                if prefer_production == -1:
                    Zlog(f'{input_str}不符合文法！')
                    exit(1)
                to_push = prefer_production[0].get_one_right_reversed(prefer_production[1])
                if to_push:  # 当产生式右部 为空时 不需要压栈
                    pda_stack.extend(to_push)
                print_status('反序压栈', prefer_production[0].get_one_str(prefer_production[1]))
            try:  # 若栈空则匹配完毕，退出循环
                top = pda_stack.pop()
            except IndexError:
                top = None
        Zlog('匹配成功！', color='GREEN', show_info=False)


def main():
    bnf = LL1('../../input/calc.txt')
    bnf.show()
    bnf.recursive_descent_parsing('a+a*a#')


if __name__ == "__main__":
    main()


# 非终结符可以使用字符串，在生成预测分析表时没有问题
# 暂时不考虑空格问题
# 暂时不考虑id应该是一个数字的问题
