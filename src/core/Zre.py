# FileName:    Zre.py
# Date:        2022-09-22 15:27:40
# Author:      zzk
# Version:     1.0
# Description: re --> NFA
from src.core.Components.NFA import *
from src.core.Components.DFA import *
from src.tools.zlog import Zlog


class Zre:
    def __init__(self):
        pass

# get sub_str between '(' ')', and return next index(ignore '(' and ')' )
# TODO(zzk): Do not support nested parentheses
def get_sub_regex(string, start):
    assert string[start] == '('
    for i in range(start, len(string)):
        if string[i] == ')':
            return start + 1, i
    assert 0  # illegal regex


# get sub_str between symbols, return index of next symbol
def get_remain_regex(string, start):
    assert string[start] == '|'
    for i in range(start + 1, len(string)):
        if string[i] in [')', '|']:
            return start + 1, i
    return start + 1, Const.MAX_RE_LEN


def check_closure(string, next_idx):
    # check if next char is *
    try:
        next_ch = string[next_idx]
    except IndexError:
        return False
    if next_ch != '*':
        return False
    return True


def re2nfa(string, start=0, end=0):
    sub_start = start
    nfa = None
    while sub_start < end:
        ch = string[sub_start]
        if ch == '(':  # get sub str
            old_sub_start, sub_start = get_sub_regex(string, sub_start)
            sub_nfa = re2nfa(string, old_sub_start, sub_start)

            # TODO(zzk): Find a better way to deal with closure
            # bad code ==============================
            if check_closure(string, sub_start + 1):
                sub_nfa.add_closure()
            # bad code ==============================

            if nfa is None:
                nfa = sub_nfa
            else:
                nfa.add_serial_graph(sub_nfa)  # be careful about this code
            sub_start += 2
        elif ch == '|':
            old_sub_start, sub_start = get_remain_regex(string, sub_start)
            sub_nfa = re2nfa(string, old_sub_start, sub_start)
            if nfa is None:
                nfa = sub_nfa
            else:
                nfa.add_parallel_graph(sub_nfa)
        elif ch == ')':
            sub_start += 1
            pass
        else:
            # bad code ==============================
            if_closure = check_closure(string, sub_start + 1)
            if if_closure:
                if nfa is None:
                    nfa = NFA(NFAState(), NFAState(), label=ch)
                else:
                    nfa.add_one_node(ch, NFAState())
                nfa.add_small_closure(ch)
                sub_start += 1
            else:
                if nfa is None:
                    nfa = NFA(NFAState(), NFAState(), label=ch)
                else:
                    nfa.add_one_node(ch, NFAState())
            # bad code ==============================
            sub_start += 1
    # nfa.show()
    # sleep(2)
    return nfa


def get_reachable(start_id, cond):
    new_ids = adj_matrix[start_id].get(cond, [])
    for _id in new_ids:
        for new_id in adj_matrix[_id].get(cond, []):
            if new_id not in new_ids:  # remove duplicates
                new_ids.append(new_id)
    # new_ids.append(start_id) # exclude start_id itself
    assert isinstance(new_ids, list)
    return new_ids


def nfa2dfa(nfa):
    # Subset Construction
    assert isinstance(nfa, NFA)
    # record all nfa_states' epsilon closure!
    epsilon_closure = {}
    # find new start id in old nfa
    dfa_start_state = DFAState()
    epsilon_closure[nfa.start.id] = get_reachable(nfa.start.id, Const.EPSILON)
    dfa_start_state.set_id(epsilon_closure[nfa.start.id] + [nfa.start.id])  # include start_id itself

    dfa = DFA(adj_matrix.get_conds(), adj_matrix.get_nfa_ends())
    dfa.add_state(dfa_start_state)
    for dfa_state in dfa.get_states():
        transform_dict = {}
        for cond in dfa.get_conds():
            cond_to_state = DFAState()
            for nfa_state in dfa_state.get_id():
                cond_to_state.extend(get_reachable(nfa_state, cond))
                for nfa_cond_to_state in cond_to_state.get_id():
                    if epsilon_closure.get(nfa_cond_to_state) is None:
                        epsilon_closure[nfa_cond_to_state] = get_reachable(nfa_cond_to_state, Const.EPSILON)
                    cond_to_state.extend(epsilon_closure[nfa_cond_to_state])

            # if cond_to_state is empty, do nothing
            if not cond_to_state.is_empty():
                # if the cond_to_state is a new dfa state, add it to dfa.states
                idx = cond_to_state.compare(dfa.get_states())
                if idx == -1:
                    cond_to_state.id = len(dfa.get_states())
                    dfa.add_state(cond_to_state)
                else:
                    cond_to_state = dfa.get_states()[idx]
                # store it to a temp dictionaries
                transform_dict[cond] = cond_to_state
        # deal with dfa transform table
        # print(transform_dict)
        # print(dfa)
        dfa.add_to_table(transform_dict)  # even the dict is empty, it should be added to table
    return dfa


# You have to Mark All End State before using this function
# TODO(zzk): father_id here is used to avoid infinite recursive loop.
#  However, it can only avoid small circle(with just two State)
def is_match(string, pos, curState, father_id):
    if pos >= len(string):
        return curState.is_end()  # recursive export
    for out_edge in curState.out_edges:
        if out_edge.label == Const.EPSILON and out_edge.to_node.id != father_id:
            if is_match(string, pos, out_edge.to_node, curState.id):  # try this way
                return True
        elif out_edge.label == string[pos]:
            if is_match(string, pos + 1, out_edge.to_node, curState.id):
                return True
    return False


# how to find end state?
# repeat edge!

def main():
    test_regex_string = ['(高裕*晟|周航)', '(a|b*)', '(a|b)*(aa|bb)(a|b)', '1(0|1)*101']
    teststr = ['a', 'abba', 'aaab']
    target = test_regex_string[1]
    nfa = re2nfa(target, 0, len(target))
    nfa.show('../../output/tmp/nfa.gv')
    dfa = nfa2dfa(nfa)
    dfa.show('../../output/tmp/dfa.gv')
    # print(is_match(teststr[2], 0, nfa.start, -1))
    Zlog('执行成功', color='GREEN', show_info=False)


if __name__ == '__main__':
    main()


# 基本功能实现（最原始的正则文法），基本的正确性实现，对闭包的处理依然很不到位
# 子集构造算法里面优化空间非常大
