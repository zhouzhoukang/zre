# -*- coding: utf-8 -*-
# FileName:     DFA.py.py
# time:         22/9/26 026 下午 6:20
# Author:       Zhou Hang
# Description:  确定状态有限自动机

# TODO: 缺乏注释, 以后补上
from src.tools.const import StateType
import graphviz


class DFAState:
    def __init__(self) -> None:
        self.id = 0  # the id is temp
        self.nfa_ids = []
        self.state_type = StateType.OTHER

    def __repr__(self) -> str:
        return str(self.id) + ':' + str(self.nfa_ids)

    def set_id(self, nfa_ids):
        self.nfa_ids = nfa_ids

    def get_id(self):
        return self.nfa_ids

    def extend(self, nfa_ids):
        assert isinstance(nfa_ids, list)
        for nfa_id in nfa_ids:
            if nfa_id not in self.nfa_ids:
                self.nfa_ids.append(nfa_id)

    def compare(self, state_list):
        for idx, state in enumerate(state_list):
            if set(self.nfa_ids) == set(state.nfa_ids):
                return idx
        return -1

    def is_end(self):
        if self.state_type == StateType.END:
            return True
        return False

    def is_empty(self):
        if len(self.nfa_ids) == 0:
            return True
        return False


class DFA:
    def __init__(self, cond_set, nfa_ends) -> None:
        assert isinstance(cond_set, set)
        self.states = []
        self.table = []
        self.cond_set = cond_set
        self.nfa_ends = nfa_ends

    def __getitem__(self, idx):
        assert isinstance(self.table[idx], DFAState)
        return self.table[idx]

    def add_state(self, dfa_state):
        assert isinstance(dfa_state, DFAState)
        # if the dfa state is end state
        for nfa_state in dfa_state.get_id():
            if nfa_state in self.nfa_ends:
                dfa_state.state_type = StateType.END
        self.states.append(dfa_state)

    def get_states(self):
        return self.states

    def get_conds(self):
        return self.cond_set

    def add_to_table(self, transform_dict):
        assert isinstance(transform_dict, dict)
        self.table.append(transform_dict)

    def __repr__(self) -> str:
        return str(self.table)

    def show(self, filename='dfa.gv'):
        g = graphviz.Digraph(filename=filename, engine='dot')
        g.attr('node', shape='circle')
        for idx, transform_dict in enumerate(self.table):
            for key in transform_dict:
                g.edge(str(idx), str(transform_dict[key].id), label=str(key), fontname='Microsoft YaHei')
                if transform_dict[key].is_end():
                    g.body.append(
                        f'{transform_dict[key].id} [shape=doublecircle]')  # Mark all end node(use double circle)
        # g.view()
        g.render(format='svg')
