# -*- coding: utf-8 -*-
# FileName:     BNF.py
# time:         22/10/8 008 下午 5:13
# Author:       Zhou Hang
# Description:  用于读取BNF文件信息，供子类使用

import re
from pathlib import Path
from typing import List, Optional, Dict, Any

from prettytable import PrettyTable

from src.tools.zerror import ZWarnning, ZAssert
from src.tools.zlog import Zlog


class Token:
    def __init__(self, name: str, is_terminal: bool, match=''):
        self.name = name
        self.is_terminal = is_terminal
        self.match = re.compile(match)

    def __str__(self):
        # return '"' + self.name + '"' + ("(T)" if self.is_terminal else "(NT)")
        return self.name

    def __repr__(self):
        # return '"' + self.name + '"' + ("(T)" if self.is_terminal else "(NT)")
        return self.name

    # TODO:为Token类添加了eq方法之后该类就变得 unhashable 了，这是什么原因？
    # def __eq__(self, other):
    #     """
    #     整个过程中Token都是唯一的，所以该函数可能并不需要
    #     :param other:
    #     :return:
    #     """
    #     if self.name == other.name:
    #         return True
    #     return False


class Production:
    def __init__(self, left: Token, rights: List[List[Token]]) -> None:
        """
        :param left: 左部
        :param rights: right[0][0] is Token 大产生式的右部是多个产生式列表的列表
        """
        self.left = left
        self.rights = rights
        self.nr_right = len(self.rights)

    def get_one_str(self, idx: int) -> str:
        """
        根据idx的值打印出其中一个产生式
        :param idx: right的索引
        :return:
        """
        ZAssert(self.nr_right > idx, "产生式右部索引下标越界")
        p = self.rights[idx]
        _str = ''
        for token in p:
            _str += f'{str(token)} '
        return f'{str(self.left)} ⟹ {_str}'

    def __str__(self) -> str:
        """该类会存放同一个left的多个产生式"""
        strs: List[str] = []
        p: List[Token]
        for p in self.rights:
            _str: str = ''
            for token in p:
                _str += f'{str(token)} '
            strs.append(_str)
        return f'{str(self.left)} ⟹ ' + '| '.join(strs)

    def get_left_name(self) -> str:
        return str(self.left)

    def get_one_right_reversed(self, idx: int) -> List[Token]:
        """根据idx的值，逆序输出产生式的右部的列表，如果右部是空，返回false"""
        if self.rights[idx][0].name == 'ε':
            return []
        return self.rights[idx][::-1]

    def gen_right_first(self):
        """生成所有产生式的第一个token"""
        for i in range(self.nr_right):
            yield self.rights[i][0]

    def gen_right(self):
        """生成所有产生式的右部列表"""
        for i in range(self.nr_right):
            yield self.rights[i]

    def gen_token_sequence(self, length: int = 2, stride: int = 1):
        """
        根据需要从产生式中获取token序列
        :param length: 获取的序列长度
        :param stride: 获取时步长
        :return:
        """
        for right in self.rights:
            right_len = len(right)
            if right_len >= length:
                for i in range(0, right_len - length + 1, stride):
                    yield right[i: i + length]


class BNF:
    def __init__(self, bnffile_path: str):
        self.productions: List[Production] = []  # 存放产生式的集合
        self.nr_production: int = 0  # 始终等于 len(self.productions)
        self.meta_symbols: List[str] = ['::=', '|']  # bnf 语法需要用到的符号
        # ====================== 维护四个符号集合 ======================
        self.non_terminals: List[str] = []
        self.non_terminal_tokens: List[Token] = []
        self.terminals: List[str] = []
        self.terminal_tokens: List[Token] = []
        # ====================== ============= ======================
        self.parser_bnffile(bnffile_path)
        self.start: Token = self._get_S()
        self.bottom: Token = Token('#', True)
        try:
            self.epsilon = self.terminal_tokens[self.terminals.index('ε')]
        except ValueError:
            self.epsilon = None

    def parser_bnffile(self, bnffile_path: str) -> None:
        """
        将文件中的原始bnf字符串读入内存生成bnf类
        :param bnffile_path: bnf 文件路径
        :return:
        """
        bnffile = Path(bnffile_path)
        ZAssert(bnffile.exists(), f"{bnffile_path} 文件不存在")
        raw_bnf = []
        # 首先将所有分行写的全部归到一行上去，支持多行bnf语法
        with bnffile.open(encoding='utf-8') as bnf_file:
            new_src, cur_idx = [], 0
            for line in bnf_file:
                line = line.lstrip()  # left strip函数也会删除换行符，如果是空行，会得到空字符串
                if len(line) == 0 or line[0] == '#':  # 忽略空行和注释
                    continue
                elif line[0] == '|':
                    new_src[cur_idx - 1] += ' ' + line
                else:
                    new_src.append(line)
                    cur_idx += 1
            for line in new_src:
                tokens = line.split()
                raw_bnf.append(tokens)

        # find all non-terminals and terminals for further use
        # 1. 所有产生式左部一定是非终结符
        nts: List[str] = [i[0] for i in raw_bnf]
        self.non_terminals: List[str] = list(set(nts))
        self.non_terminals.sort(key=nts.index)  # 这里只是为了调试方便，加不加都行

        # 2. 所有产生式中除了元字符和非终结符的就是终结符
        ts: List[str] = [token for production in raw_bnf for token in production if
                         token not in self.non_terminals and token not in self.meta_symbols]
        self.terminals: List[str] = list(set(ts))
        self.terminals.sort(key=ts.index)  # 这里只是为了调试方便，加不加都行

        # 3. 为每一个标识符生成独一无二的Token对象，下面只需要查表使用即可
        self.non_terminal_tokens: List[Token] = [Token(item, False) for item in self.non_terminals]
        self.terminal_tokens = [Token(item, True) for item in self.terminals]

        # 4. 开始处理原始bnf文本, 生成Production
        for production in raw_bnf:
            left, right, temp_right = None, [], []
            for idx, token in enumerate(production):
                if idx == 0:  # left
                    left = self._get_token_by_name(token)
                    ZAssert(left is not None, "panic")  # 永远可以查到
                    continue
                if idx == 1:  # symbol ::= 直接忽略
                    continue
                if token == '|':
                    if temp_right:
                        right.append(temp_right)
                        temp_right = []
                else:
                    right_token = self._get_token_by_name(token)
                    temp_right.append(right_token)
            right.append(temp_right)  # 最后一个生成项
            _production = Production(left, right)
            self._append_productions(_production)

    def _get_token_by_name(self, token_name: str) -> Optional[Token]:
        """通过token名获得相应的独一无二的Token对象"""
        try:
            idx = self.non_terminals.index(token_name)
            return self.non_terminal_tokens[idx]
        except ValueError:
            try:
                idx = self.terminals.index(token_name)
                return self.terminal_tokens[idx]
            except ValueError:
                return None

    def _append_productions(self, production: Production) -> None:
        self.productions.append(production)
        self.nr_production += 1

    def _insert_productions(self, pos: int, production: Production) -> None:
        self.productions.insert(pos, production)
        self.nr_production += 1

    def _get_S(self) -> Token:
        """获得最开始的Token对象"""
        return self.productions[0].left

    def get_non_terminal_num(self):
        return len(self.non_terminals)

    def get_terminal_num(self):
        return len(self.terminals)

    def add_token(self, new_token: Token):
        if new_token.is_terminal:
            self.terminals.append(new_token.name)
            self.terminal_tokens.append(new_token)
        else:
            self.non_terminals.append(new_token.name)
            self.non_terminal_tokens.append(new_token)

    def _find_matched_terminal(self, _str: str):
        """
        从终结符集合中找到可能匹配到给定字符串的那个
        TODO: 后期应该使用正则匹配,因为有些终结符可以表达很多东西
        :param _str:
        :return:
        """
        for terminal in self.terminals:
            if_matched = _str.startswith(terminal)  # 此处应该使用正则
            if if_matched:
                matched_terminal = terminal
                return matched_terminal
        Zlog(f'{_str}不符合文法！')
        exit(-1)  # should never reach here

    def _find_matched_terminal_token(self, _str):
        """
        此函数的存在是由于我没想清楚PDA栈里面要放些什么
        :param _str:
        :return:
        """
        for terminal_token in self.terminal_tokens:
            if_matched = _str.startswith(terminal_token.name)  # 此处应该使用正则
            if if_matched:
                matched_terminal = terminal_token
                return matched_terminal
        Zlog(f'串 {_str} 找不到匹配的token，不符合文法！')
        exit(1)  # should never reach here

    def _for_each_production(self):
        """
        nnd 每次遍历还要两重循环，我裂开了
        :return:
        """
        for production in self.productions:
            for right in production.gen_right():
                yield production.left, right

    def show(self):
        for production in self.productions:
            print(production)


class FFBNF(BNF):
    """
    拥有计算首符集First和随符集Follow功能的扩展类
    用于服务LL(1)算法和SLR(1)算法,这两个都需要用到首符集和随符集
    """
    def __init__(self, bnffile_path):
        super(FFBNF, self).__init__(bnffile_path)
        self.first: Dict[str, Optional[List, set][Token]] = {}  # 每一个value都是set
        self.follow: Dict[str, Optional[List, set][Token]] = {}  # 每一个value都是set

    def calc_first(self, epoch_time=10):
        """
        calculate the first character set 求首符集
        :return:
        """
        for production in self.productions:
            self.first[production.get_left_name()] = []
            for right_first in production.gen_right_first():
                # 先把每个产生式的右部第一个token加入到对应left的首符集中
                # 由于我的实现方法有点不大一样，这里不用考虑加入的是不是终结符
                self.first[production.get_left_name()].append(right_first)

        # 这里肯定可以递归，但是我想不出来, 多迭代几次肯定行
        # 我不信求first的深度会超过10，如果超过了就加大这个值
        for epoch in range(epoch_time):
            for key in self.first:
                for right_first in self.first[key]:
                    if not right_first.is_terminal:
                        self.first[key].remove(right_first)
                        self.first[key].extend(self.first[right_first.name])
        # 由于没有递归，不能保证所有情况都正确，这里浅做个判断
        for key in self.first:
            for right_first in self.first[key]:
                if not right_first.is_terminal:
                    Zlog('求首符集出现错误，增大epoch_time或许有效')
                    exit(-1)

        # 去重
        for key in self.first:
            self.first[key] = set(self.first[key])

    def calc_follow(self):
        """
        calculate the companion set
        将栈底符加入FOLLOW(开始符)中
        – 按照下面两个规则不断迭代，直到所有的FOLLOW集合都不再增长为止
        1. 如果存在产生式 A → αBβ，那么FIRST(β)中所有非ε的符号都加入FOLLOW(B)中
        2. 如果存在一个产生式 A →αB，或者 A → αBβ且FIRST(β)包含ε，
        那么FOLLOW(A)中所有符号都加入FOLLOW(B)中
        :return:
        """
        for nt_t in self.non_terminals:
            self.follow[nt_t] = set()  # 新建好所有的follow集合
        self.follow[self.start.name].add(self.bottom)  # 将栈底符号添加进开始符号

        restart = True
        while restart:
            restart = False
            # 一旦出现某个follow集增长，就继续迭代
            for left, right in self._for_each_production():
                for i, token in enumerate(right):
                    if not token.is_terminal:
                        early_stop, beta, old_length = True, None, len(self.follow[token.name])
                        try:
                            beta = right[i + 1]
                            if beta.is_terminal:  # 情况1，并且β是终止符
                                self.follow[token.name].add(right[i + 1])  # ---直接把β添加进来即可---
                            else:  # 情况1，并且β是非终止符
                                early_stop = False
                        except IndexError:  # 情况2 不存在β
                            self.follow[token.name] = self.follow[token.name].union(
                                self.follow[left.name])  # ---将产生式左部随符集全部添加进来---
                        if not early_stop:  # 情况1，β是非终止符
                            self.follow[token.name] = self.follow[token.name].union(
                                set(self.first[beta.name]))  # ---把β首符集全部添加进来---
                            try:  # 情况2 β非终止且first(β) 包含ε
                                # print(self.follow[token.name])
                                self.follow[token.name].remove(self.epsilon)  # ---删除β中的ε---
                                self.follow[token.name] = self.follow[token.name].union(
                                    self.follow[left.name])  # ---把β首符集全部添加进来---
                            except ValueError and KeyError:  # 情况1 β非终止符且不包含ε
                                pass
                        new_length = len(self.follow[token.name])
                        if old_length != new_length:
                            restart = True

    def show_ff(self):
        if self.first == {} or self.follow == {}:
            return
        table = PrettyTable(['非终结符', '首符集(FIRST)', '随符集(FOLLOW)'])
        for key in self.first:
            table.add_row([key, self.first[key], self.follow[key]])
        print(table)

    def str_ff(self):
        """
        为qt界面使用,将首符集和随符集的内容返回出来
        :return:
        """
        pass


def main():
    bnf = BNF('../../../input/calc_2.txt')
    bnf.show()


if __name__ == "__main__":
    main()

# 不支持如下嵌套 | 的语法！
# transform   ::= '/' regex '/' (format | text)+ '/' options
# 不支持如下尖括号语法！
# control sequence ::= <\033[> + (p1;)(p2)...(pn) + <a letter>
# 终结符如果是字符串，不要加引号
# epsilon 请使用 ε 其他一律不行
# 支持在bnf文件中使用 # 的注释，但是必须在行首，不支持行内注释
