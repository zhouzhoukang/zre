from src.tools.const import Const
import graphviz
from src.core.Components.Graph import FANode


class AdjMatrix:
    """
    邻接矩阵，换了一种数据结构来存储NFA
    """
    def __init__(self) -> None:
        self.matrix = []
        self.nr_state = 0
        self.conds = set()  # 记录所有转换条件
        self.nfa_ends = []  # record all end state

    def expand_matrix(self, size):
        for i in range(size - self.nr_state):
            self.matrix.append({})
        self.nr_state = size

    def __getitem__(self, _id):
        if _id >= self.nr_state:
            self.expand_matrix(_id + 1)
        res = self.matrix[_id]
        return res

    def __len__(self):
        return len(self.matrix)

    def __repr__(self):
        return self.matrix

    def add_edge(self, from_id, label, toid):
        if self[from_id].get(label) is None:
            self[from_id][label] = []
        self[from_id][label].append(toid)
        if label != Const.EPSILON:
            self.conds.add(label)  # record every cond except epsilon

    def get_conds(self):
        return self.conds

    def add_nfa_ends(self, end_id):
        self.nfa_ends.append(end_id)

    def get_nfa_ends(self):
        return self.nfa_ends


class NFAState(FANode):
    """专门为NFA封装的状态类"""
    def __init__(self) -> None:
        super(NFAState, self).__init__()

    def point_to(self, label: str, _next):
        assert isinstance(_next, NFAState)
        super(NFAState, self).point_to(label, _next)
        # maintain a matrix for nfa2dfa
        adj_matrix.add_edge(self.id, label, _next.id)

    # 从当前节点开始, 遍历寻找所有epsilon闭包(包括反方向), 作为END节点
    # 有向有环图遍历起来会死循环, 这里写的不好, 根本方法是换个数据结构
    def find_all_end(self):
        if self.is_end():
            return
        self.set_end()
        adj_matrix.add_nfa_ends(self.id)
        for in_edge in self.in_edges:
            if in_edge.label == Const.EPSILON:
                in_edge.from_node.find_all_end()  # TODO(zzk): Bad way to find end state
        for edge in self.out_edges:
            if edge.label == Const.EPSILON:
                edge.to_node.find_all_end()


class NFA:
    def __init__(self, start, end, label=Const.EPSILON) -> None:
        self.start = start
        self.end = end
        start.point_to(label, self.end)

    def add_parallel_graph(self, nfa):
        newStart = NFAState()
        newEnd = NFAState()
        newStart.point_to(Const.EPSILON, self.start)
        newStart.point_to(Const.EPSILON, nfa.start)
        self.end.point_to(Const.EPSILON, newEnd)
        nfa.end.point_to(Const.EPSILON, newEnd)
        self.start = newStart
        self.end = newEnd

    def add_serial_graph(self, nfa):
        self.end.point_to(Const.EPSILON, nfa.start)
        self.end = nfa.end

    def add_closure(self):
        """
        图开始节点指向结束节点的闭包
        :return:
        """
        self.end.point_to(Const.EPSILON, self.start)
        self.start.point_to(Const.EPSILON, self.end)

    def add_small_closure(self, ch):
        """
        自己指向自己的闭包
        :param ch:
        :return:
        """
        self.end.point_to(ch, self.end)

    def add_one_node(self, label, _next):
        self.end.point_to(label, _next)
        self.end = _next

    def mark_all_end(self):
        self.end.find_all_end()

    def show(self, filename='nfa.gv'):
        self.mark_all_end()
        has_painted = {}  # record every edge to avoid repeat paint
        stack = []  # Let's do some beautiful BFS!
        g = graphviz.Digraph(filename=filename, engine='dot')
        g.attr('node', shape='circle')
        p = self.start
        while p is not None:
            for edge in p.out_edges:
                toStateID = edge.get_to_node_id()
                if toStateID not in has_painted.get(p.id, []):
                    g.edge(str(p.id), str(toStateID), label=edge.label, fontname="Microsoft YaHei")
                    if edge.to_node.is_end():
                        g.body.append(f'{toStateID} [shape=doublecircle]')  # Mark all end node(use double circle)
                    if has_painted.get(p.id) is None:
                        has_painted[p.id] = []
                    has_painted[p.id].append(toStateID)
                    stack.append(edge.to_node)
            # It's convenient to use try-except block when doing BFS
            try:
                p = stack.pop()
            except IndexError:
                p = None
        # g.view()
        g.render(format='svg')


adj_matrix = AdjMatrix()  # 一个全局的邻接表
