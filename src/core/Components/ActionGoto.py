# -*- coding: utf-8 -*-
# FileName:     ActionGoto.py
# time:         22/10/24 024 下午 5:36
# Author:       Zhou Hang
# Description:  I don't want to write

from src.tools.zlog import Zlog
from prettytable import PrettyTable


class ShiftState:
    """AG表中移进项目"""

    def __init__(self, next_state: int):
        assert next_state > 0 or next_state == -2  # -2用来标记下一个状态暂时不确定
        self.next_state = next_state

    def __repr__(self):
        return Zlog(f'Shift{self.next_state}', color='YELLOW', if_print=False, show_info=False).get_str()

    def get_next_state(self):
        return self.next_state

    def set_next_state(self, next_state):
        self.next_state = next_state

    def __eq__(self, other):
        if isinstance(other, type(self)):
            if self.next_state == other.next_state:
                return True
        return False


class ReduceProject:
    """AG表中规约某个项目"""

    def __init__(self, project):
        # assert isinstance(project, Project)  # 这里找不到Project类,仅起注释作用
        self.project = project

    def __repr__(self):
        return Zlog(f'{self.project}', color="CYAN", if_print=False, show_info=False).get_str()

    def get_next_project(self):
        return self.project

    def __eq__(self, other):
        if isinstance(other, type(self)):
            if self.project == other.project:
                return True
        return False


class GOTOState:
    """Goto状态不可能会冲突"""

    def __init__(self, next_state):
        assert isinstance(next_state, int)
        assert next_state > 0 or next_state == -2  # -2用来标记下一个状态暂时不确定
        self.next_state = next_state

    def __repr__(self):
        return str(self.next_state)

    def get_next_state(self):
        return self.next_state

    def set_next_state(self, next_state):
        self.next_state = next_state

    def __eq__(self, other):
        if isinstance(other, type(self)):
            if self.next_state == other.next_state:
                return True
        return False


# 起了一些稀奇古怪的名字
class AGACC:
    """ActionGoto Accept"""

    def __repr__(self):
        return Zlog("ACC", color='GREEN', if_print=False, show_info=False).get_str()

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return True
        return False


class AGERR:
    """ActionGoto Error"""

    def __repr__(self):
        return Zlog("Error", color='RED', if_print=False, show_info=False).get_str()

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return True
        return False


class ActionGotoItem:
    """每一个表项都可能发生冲突并且有可能是[ShiftState, ReduceProject, AGACC, AGERR]"""
    SHIFT = 0
    REDUCE = 1
    GOTO = 2
    ACC = 3
    ERR = 4

    def __init__(self, _type, state_project=-1):
        self.item = None

        if _type == 'shift':
            self.type = ActionGotoItem.SHIFT
            self.item = ShiftState(state_project)
        elif _type == "reduce":  # 规约的时候要指定的是产生式
            self.type = ActionGotoItem.REDUCE
            self.item = ReduceProject(state_project)
        elif _type == "GOTO":
            self.type = ActionGotoItem.GOTO
            self.item = GOTOState(state_project)
        elif _type == "ACC":
            self.type = ActionGotoItem.ACC
            self.item = AGACC()
        elif _type == "ERR":
            self.type = ActionGotoItem.ERR
            self.item = AGERR()
        else:
            Zlog('错误的ActionGoto表项, _type应该是["ACTION", "GOTO", "ACC", "ERR"]')
            exit(-1)

    def is_shift(self):
        return self.type == ActionGotoItem.SHIFT

    def is_reduce(self):
        return self.type == ActionGotoItem.REDUCE

    def is_action(self):
        return self.is_shift() or self.is_reduce()

    def is_goto(self):
        return self.type == ActionGotoItem.GOTO

    def is_acc(self):
        return self.type == ActionGotoItem.ACC

    def is_error(self):
        return self.type == ActionGotoItem.ERR

    def get_next_state(self):
        # assert self.is_shift() or self.is_goto()
        return self.item.get_next_state()  # 如果这里报错找不到该函数,说明初始化的时候错误

    def get_next_project(self):
        assert not self.is_shift()
        return self.item.get_next_project()  # 如果这里报错找不到该函数,说明初始化的时候错误

    def set_next_state(self, next_state):
        assert self.type == ActionGotoItem.SHIFT or self.type == ActionGotoItem.GOTO
        self.item.set_next_state(next_state)

    def __eq__(self, other):
        if self.item == other.item:
            assert self.type == other.type
            return True
        return False

    def __repr__(self):
        return str(self.item)


class ActionGotoItems:
    """为了支持action goto表项的冲突"""

    def __init__(self, *items):
        """
        支持传入多个ActionGotoItem，或者一个ActionGotoItem的列表来初始化
        :param items:
        """
        self.items = []
        self.count = 0  # 为了实现该类的可迭代
        if isinstance(items[0], ActionGotoItem):
            self.items = list(items)
        elif isinstance(items[0], list):
            # 列表形式传入了一堆item
            assert isinstance(items[0][0], ActionGotoItem)
            self.items.extend(items[0])

    def has_conflict(self):
        """判断当前表项是否存在冲突"""
        if len(self.items) > 1:
            return True
        return False

    def is_error_item(self):
        if len(self.items) == 1 and self.items[0].is_error():
            return True
        return False

    def is_acc_item(self):
        """TODO:想清楚了acc状态会不会发生冲突"""
        if len(self.items) == 1 and self.items[0].is_acc():
            return True
        return False

    def is_goto_item(self):
        """
        goto状态不会发生冲突
        :return:
        """
        if len(self.items) == 1 and self.items[0].is_goto():
            return True
        return False

    def add_item(self, item):
        """
        item可以是单独的ActionGotoItem元素也可以是列表
        :param item:
        :return:
        """
        if not isinstance(item, list):
            item = [item]
        assert isinstance(item[0], ActionGotoItem)
        for i in item:
            for has_added in self.items:
                if not i == has_added:  # 不能重复添加
                    self.items.append(i)

    def __repr__(self):
        ret = ''
        for item in self.items:
            ret += str(item)
            ret += '\n'
        ret = ret.rstrip()
        return ret

    def __iter__(self):
        return self

    def __next__(self):
        if self._count < len(self.items):
            self._count += 1
            return self.items[self._count - 1]
        else:
            self._count = 0
            raise StopIteration

    def __getitem__(self, idx):
        assert isinstance(self.items[idx], ActionGotoItem)
        return self.items[idx]


class ActionGoto:
    def __init__(self, state_number, terminals, bottom, non_terminals):
        """所有参数都不是token, 就是普通字符串"""
        self.state_number = state_number
        self.terminals = terminals
        self.bottom = bottom
        self.non_terminals = non_terminals
        self.table = []  # 从0开始, 下标代表状态号, 每个元素都是字典, 存放对应Item对象，可以有多个item
        for i in range(state_number):
            init = {}
            for t in self.terminals + [self.bottom]:
                init[t] = ActionGotoItems(ActionGotoItem("ERR"))
            for nt in self.non_terminals:
                init[nt] = ActionGotoItems(ActionGotoItem("ERR"))
            self.table.append(init)

    def __getitem__(self, index) -> dict:
        """
        :param index:
        :return: 一定是一个字典,表明当前状态下的下一步应该怎么走
        """
        if index >= self.state_number:
            Zlog(f'对ActionGoto表的引用不当:index:{index} (应该小于{self.state_number})')
            raise IndexError
        return self.table[index]

    def set_item(self, state, t_nt, item):
        """
        如果原本的item是Error，就直接set，如果不是就add
        :param state: 状态号
        :param t_nt: 对应输入字符
        :param item: 要添加的动作
        :return:
        """
        if self[state][t_nt].is_error_item():
            self[state][t_nt] = ActionGotoItems(item)
        else:
            self[state][t_nt].add_item(item)

    def set_items(self, state, t_nt, items):
        assert isinstance(items, list)
        assert isinstance(items[0], ActionGotoItem)
        if self[state][t_nt].is_error_item():
            self[state][t_nt] = ActionGotoItems(items)
        else:
            self[state][t_nt].add_item(items)

    def set_item_multi_cols(self, state, t_nts, item):
        """
        在某行的多个列设置item, 适用LR(0)分析时在一整行添加reduce项目
        :param state:
        :param t_nts:
        :param item:
        :return:
        """
        assert isinstance(t_nts, list)
        for t_nt in t_nts:
            self.set_item(state, t_nt, item)

    def get_item(self, state, t_nt) -> ActionGotoItems:
        assert isinstance(self[state][t_nt], ActionGotoItems)
        return self[state][t_nt]

    def show(self):
        to_walk = self.terminals + [self.bottom] + self.non_terminals
        table = PrettyTable(['状态'] + to_walk)
        for i in range(self.state_number):
            items = [i]
            for key in self[i]:
                items += [str(self[i][key])]
            table.add_row(items)
        print(table)


def main():
    t = ActionGoto(5, ['a', 'b', 'c'], '#', ['A', 'B'])
    t.set_item(0, 'a', ActionGotoItem('shift', 1))
    t.show()


if __name__ == "__main__":
    main()
