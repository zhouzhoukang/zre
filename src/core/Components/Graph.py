# -*- coding: utf-8 -*-
# FileName:     Graph.py
# time:         22/10/15 015 下午 3:08
# Author:       Zhou Hang
# Description:  将NFA/DFA图中的一些信息抽象出来 TODO:失败
from typing import List

from src.tools.const import StateType


class FANode:
    """每个节点都需要记录所有的入边和出边"""
    id_count = 0  # a static var

    def __init__(self) -> None:
        self.id = FANode.id_count  # automatically increase state number
        FANode.id_count += 1
        self.out_edges: List[FAEdge] = []  # one state could have a lot of out_edges, record all of them
        self.in_edges: List[FAEdge] = []  # record every in_edge that point to self
        self.type = StateType.OTHER

    def is_end(self) -> bool:
        """该节点是不是终结态"""
        return self.type == StateType.END

    def set_end(self):
        """设置当前节点为终结态"""
        self.type = StateType.END

    def point_to(self, label: str, _next) -> None:  # TODO 这里没法设置类型
        """
        设置该节点的指向节点
        :param label: 标签
        :param _next: 下一个节点
        :return:
        """
        assert isinstance(_next, FANode)
        edge = FAEdge(self, label, _next)
        self.out_edges.append(edge)
        _next.in_edges.append(edge)  # record from edge

    def __str__(self):
        return f"{self.id}"


class FAEdge:
    def __init__(self, from_node: FANode, label: str, to_node: FANode) -> None:
        assert isinstance(to_node, FANode)
        self.from_node: FANode = from_node  # point to last State
        self.label: str = label
        self.to_node: FANode = to_node  # point to next State

    def get_from_node_id(self) -> int:
        return self.from_node.id

    def get_to_node_id(self) -> int:
        return self.to_node.id

    def __str__(self):
        return f'{str(self.from_node)} {self.label}--> {str(self.to_node)}'


class graph:
    def __init__(self):
        pass


def main():
    pass


if __name__ == "__main__":
    main()
