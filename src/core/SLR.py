# -*- coding: utf-8 -*-
# FileName:     SLR.py
# time:         22/10/13 013 下午 10:33
# Author:       Zhou Hang
# Description:  LR分析法, LR(0)分析器几乎没有作用, 所以这里是SLR(1)分析器
import graphviz
from src.core.Components.BNF import *
from src.core.Components.ActionGoto import *
from src.tools.misc import drop_color
from src.tools.zerror import ZPass


class Project:
    """
    一个项目就是把一个产生式转化为很多带点的产生式
    该类仅仅用于LR(0)算法, 如果要使用LR(1),需要添加一个前看符号
    """
    def __init__(self, _id, left, right, dot_pos):
        """
        :param _id: 每一个项目要对应一个状态
        :param left: 项目左部
        :param right: 项目右部
        :param dot_pos: 规则:对于一个右部ABC, 有 (1)A(2)B(3)C(4), 表示原点的位置
        """
        assert isinstance(left, Token)
        assert isinstance(right[0], Token)
        self.id = _id
        self.left = left
        self.right = right
        self.right_len = len(self.right)
        self.dot_pos = dot_pos
        # 记录小圆点后面的那个Token, 没有就是None
        self.behind_dot = self.right[self.dot_pos - 1] if self.dot_pos <= self.right_len else None
        self.before_dot = self.right[self.dot_pos - 2] if self.dot_pos > 1 else None

    def is_dot_in_end(self):
        """
        判断小圆点在不在最后
        :return:
        """
        return self.dot_pos == len(self.right) + 1

    def behind_dot_is_terminal(self):
        """
        判断小圆点后面的符号是不是终结符
        将小圆点在最后视为终结符
        """
        if self.behind_dot is None:
            return None
        assert isinstance(self.behind_dot, Token)
        return self.behind_dot.is_terminal

    def is_accept(self, old_start):
        """判断该项目是不是ACC状态"""
        if self.behind_dot is None and self.before_dot == old_start:
            assert self.right_len == 1
            return True
        return False

    def __repr__(self):
        _str = ''
        has_dot = False
        for idx, token in enumerate(self.right):
            if idx + 1 == self.dot_pos:  # 在左边加小圆点
                _str += f'· {token.name} '
                has_dot = True
            else:
                _str += f'{token.name} '
        if not has_dot:
            _str += '· '
        # _str += f'|{self.look_ahead}'
        # return f'{self.id: ^3}: {self.left.name} ⟹ {_str}'  # 显示项目编号
        return f'{self.id}:{self.left.name}⟹{_str}'  # 不显示项目编号,并且删除了空格

    def __eq__(self, other):
        """
        凭借id号来确定两者是否相同
        因为规定id号应该对应唯一的项目
        :param other:
        :return:
        """
        assert isinstance(other, Project)
        # if self.id == other.id and self.look_ahead == other.look_ahead:
        if self.id == other.id:
            assert self.left.name == other.left.name
            return True
        return False


TEMPLATE = '''<
<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="4">
{}
</TABLE>>'''
ONE_LINE = '''<TR><TD>{}</TD></TR>'''


class ProjectDFANode:
    """也就是项集(项目集), 表示DFA中的一个状态, 会包括 1-n个项目"""
    def __init__(self, _id):
        self.id = _id  # 这里是DFANode编号，与项目编号区分开
        self.projects = []
        self._count = 0  # 为迭代器准备, 也只会在迭代器中使用
        self.out_edge = {}  # 使用字典描述
        # self.is_accept = None

    def add_project(self, to_add_projects) -> bool:
        """
        去重添加元素! to_add_projects 是待添加的项目
        :param to_add_projects: 待添加到该节点的的项目, 可以是单元素也可以是列表
        :return: 若集合没有变化就返回False, 否则返回True并进行进一步求闭包
        """
        proj_len = len(self.projects)
        if not isinstance(to_add_projects, list):
            to_add_projects = [to_add_projects]
        for to_add_project in to_add_projects:
            if_add = True
            for proj in self.projects:
                if proj == to_add_project:
                    if_add = False  # 一旦发现列表中已经有了就不添加
                    break
            if if_add:
                self.projects.append(to_add_project)
        return proj_len != len(self.projects)

    def conflict_detect(self, old_start):
        """
        检测该状态是否既能规约又能移入, 将该节点全部的有可能的action都返回出来
        :param old_start: 用于检查是不是ACC action
        :return: 一个action列表
        """
        ret = []
        for project in self.projects:
            if project.is_dot_in_end():
                # 存在reduce或acc动作, 进一步判断究竟是哪个
                if project.is_accept(old_start):
                    ret.append((ActionGotoItem('ACC'), ))
                else:
                    ret.append((ActionGotoItem('reduce', project), ))
            else:
                # 存在shift或goto动作, 进一步判断究竟是哪个
                if project.behind_dot.is_terminal:
                    # 这里填写-2表示待定，在本节点内并不知道下一个状态是什么, 将小圆点后面的token返回出去用作下一个状态判断
                    ret.append((ActionGotoItem('shift', -2), project.behind_dot.name))
                else:
                    ret.append((ActionGotoItem('GOTO', -2), project.behind_dot.name))
        return ret

    def add_edge(self, label, next_project):
        self.out_edge[label] = next_project

    def is_empty(self):
        if len(self.projects) == 0:
            return True
        return False

    def set_id(self, _id):
        """
        若要调用这个方法，id必须是None，说明是先建立后注册的
        :param _id:
        :return:
        """
        assert self.id == -1
        self.id = _id

    def exist_out_edge(self, label):
        """
        判断该dfa节点的所有出边中是否存在一个给定label的边
        :param label:
        :return:
        """
        for key in self.out_edge:
            if key == label:
                return self.out_edge[key]
        return None

    # def __repr__(self):
    #     _str = f'{self.id: ^3} --- '
    #     for proj in self.projects:
    #         _str += f'{proj} || '
    #     return _str

    def __repr__(self):
        lines = ''
        lines += ONE_LINE.format(str(self.id))
        for proj in self.projects:
            one_line = ONE_LINE.format(str(proj))
            lines += one_line
        ret = TEMPLATE.format(lines)
        return ret

    def _get_id_set(self):
        """
        为eq方法做准备，得到一个id的集合
        这里是通过项目的id来判断两个DFANode是否一样，这在LR1中不合适
        :return:
        """
        ret = set()
        for proj in self.projects:
            ret.add(proj.id)
        return ret

    def __eq__(self, other):
        assert isinstance(other, ProjectDFANode)
        if len(self) != len(other):
            return False
        return self._get_id_set() == other._get_id_set()

    def __len__(self):
        return len(self.projects)

    def __iter__(self):
        return self

    def __next__(self):
        if self._count < len(self.projects):
            self._count += 1
            return self.projects[self._count - 1]
        else:
            self._count = 0
            raise StopIteration


class SLRBNF(FFBNF):
    """用于编写LR(0)和SLR(1)算法的类"""
    def __init__(self, bnffile_path):
        super(SLRBNF, self).__init__(bnffile_path)
        self.a_action = []
        self.a_now_input_str = []
        self.a_op_stack = []
        self.a_state_stack = []
        self.projects = []
        self._outreach_grammar()  # 生成拓广文法
        self.calc_first()
        self.calc_follow()
        self._transform_to_projects()  # 生成项目
        self.dfa_id = 0  # 永远记录下一个待分配的DFANode编号
        self.dfa, self.dfa_id = ProjectDFANode(self.dfa_id), self.dfa_id + 1  # 在该类中嵌入一个识别活前缀的DFA的根节点
        self.all_dfa_nodes = [self.dfa]  # 维护一个所有dfa节点的集合
        self._dfa_init()  # 初始化第一个DFA节点
        self._gen_dfa()
        self.action_goto = ActionGoto(self.dfa_id, self.terminals, self.bottom.name, self.non_terminals)
        self._gen_action_goto()

    def _outreach_grammar(self):
        """
        添加一个 S' -> S 的产生式, 为后面铺垫
        注意原始文法中不能有S'这个单词了
        :return:
        """
        self.old_start, s = self.start, self.start
        new_s = Token('S\'', False)
        self.add_token(new_s)  # 将新开始符添加进去
        self.start = new_s
        self._insert_productions(0, Production(new_s, [[s]]))

    def _transform_to_projects(self) -> None:
        """
        将原来的production结构改写, self.productions 里每一个右部都能拆分成很多个项目
        :return:
        """
        proj_id = 1
        for left, right in self._for_each_production():
            for i in range(1, len(right) + 2):
                self.projects.append(Project(proj_id, left, right, i))
                proj_id += 1

    def _find_next_project(self, project):
        """
        在当前的project基础上直接找到下一个project
        依据给定的项目找到和该项目一样并且小圆点往后挪动一位
        :param project:
        :return: 一个项目，是一个！不是一个列表
        """
        possibles = self._find_project(project.left.name, project.dot_pos + 1)  # 先找出可能的
        for possible in possibles:
            if possible.before_dot is project.behind_dot:
                return possible

    def _find_project(self, left_name, dot_pos):
        """
        根据左部名称和小圆点的位置来寻找对应的项目.
        TODO:关键是符合条件的可能有很多, 要全部找出来!
        :param left_name: 待查找的左部名称
        :param dot_pos: 带查找的小圆点位置
        :return:
        """
        proj_list = []
        for proj in self.projects:
            if proj.left.name == left_name and proj.dot_pos == dot_pos:
                proj_list.append(proj)
        return proj_list

    def add_project_to_dfaNode(self, dfa_node, projects):
        """
        将一个列表里面的所有项目去重的加入到dfa节点中, 每一个项目符合条件的相关项目也添加进来
        实际上就是在做 项集闭包 (CLOSURE), TODO:这里要递归求闭包 https://cs.nju.edu.cn/changxu/2_compiler/slides/Chapter_4.pdf
        前看符号等到LR(1)文法再研究
        :param dfa_node: 待添加的dfa节点
        :param projects: 待添加的所有项目
        :return:
        """
        assert isinstance(dfa_node, ProjectDFANode)
        # assert dfa_node.is_empty()  # 如果是第一次添加, 一定是全部添加进去, 并且还需要进一步求闭包
        has_added_projects = list(projects)
        projects = []
        while dfa_node.add_project(has_added_projects):  # 求项集闭包算法,直到待添加的集合大小不变才停止计算
            # 将相关的项目也添加进来
            for project in has_added_projects:
                if project.behind_dot_is_terminal() is False:
                    to_add_projects = self._find_project(project.behind_dot.name, 1)  # to_add_proj是一个列表
                    projects.extend(to_add_projects)
            has_added_projects = list(projects)

    def _dfa_init(self):
        """
        初始化dfa初始节点
        :return:
        """
        self.add_project_to_dfaNode(self.dfa, self._find_project('S\'', 1))  # 这里_find_project一定是唯一的

    def _node_is_exist(self, _node):
        """
        判断这个新的节点对象有没有生成过，有没有被加入到all_dfa_nodes中
        :param _node: 待查找的节点
        :return: 若找到了就把那个值返回，否则返回None
        """
        assert isinstance(_node, ProjectDFANode)
        for dfa_node in self.all_dfa_nodes:
            if _node == dfa_node:
                return dfa_node
        return None

    def _gen_dfa(self):
        """
        从初始节点开始一步步构造识别活前缀的DFA
        当多个节点中存在多个项目的小圆点后token一样, 同一个状态会产生两个DFA Node而无法识别
        但是这个问题好像不解决也不影响
        :return:
        """
        old_wait_for_walk, wait_for_walk = [self.dfa], []
        restart = True
        while restart:
            restart = False
            for idx, node_to_walk in enumerate(old_wait_for_walk):  # 遍历每一个新增的dfa节点
                for proj in node_to_walk:  # 遍历dfa节点中的每一个项目
                    # 此时应该分三种情况，该项目小圆点后面一个是终结符/非终结符/无符号
                    if proj.behind_dot_is_terminal() is None:  # 小圆点已经在最后了
                        continue
                    else:  # 小圆点后面是终结符/非终结符，这里不加区分
                        behind_dot_token = proj.behind_dot
                        # assert behind_dot_token in self.terminal_tokens
                        # 该项目应该读入一个终结符/非终结符进入下一个dfa节点
                        # 这里behind_dot_token有可能已经存在于out_edge中了！先判断这种情况
                        if_has_exist = node_to_walk.exist_out_edge(behind_dot_token.name)
                        if if_has_exist is not None:  # 加入到一个已经存在的节点中
                            assert isinstance(if_has_exist, ProjectDFANode)
                            self.add_project_to_dfaNode(if_has_exist, [self._find_next_project(proj)])
                        else:
                            # 应该先试探着新建一个dfa节点，检查该dfa节点是否已经存在
                            # 万一得到一个输入之后结果是自己，或者是一个已经存在的dfa Node怎么办
                            # 尝试生成
                            new_dfa = ProjectDFANode(-1)
                            # TODO:以下代码建立的dfa节点并不是完全体！因为上一个状态下还有一些没有被遍历到的产生式
                            self.add_project_to_dfaNode(new_dfa, [self._find_next_project(proj)])  # 闭包的添加
                            exist = self._node_is_exist(new_dfa)
                            if exist is None:  # 不存在该节点
                                new_dfa.set_id(self.dfa_id)
                                self.dfa_id += 1
                                self.all_dfa_nodes.append(new_dfa)  # 注册
                                wait_for_walk.append(new_dfa)  # 只有没有遍历过的节点才需要加入到这个集合中，等待下一轮
                            else:
                                new_dfa = exist
                            # 将新建的dfa Node 连接到当前node_to_walk上，TODO:具体连接方法有待商榷
                            node_to_walk.add_edge(behind_dot_token.name, new_dfa)
                if idx >= len(old_wait_for_walk) - 1:  # 这里采用restart模式来循环
                    if len(wait_for_walk) == 0:
                        pass
                    else:
                        old_wait_for_walk = list(wait_for_walk)
                        wait_for_walk = []
                        restart = True

    def _for_each_dfa_node(self):
        """
        深度优先遍历节点
        生成器深得我心，这样做可以把图当成数组一样去遍历！
        """
        has_reached = []
        depth_stack = [self.dfa]
        yield self.dfa
        while len(depth_stack) != 0:
            node = depth_stack.pop()  # 这里要确保栈中数据一定是没有遍历过的
            has_reached.append(node)
            for key in node.out_edge:
                cur_node = node.out_edge[key]  # 判断下这个cur_node是否曾经入过栈
                if cur_node not in has_reached:
                    depth_stack.append(cur_node)
                    yield cur_node

    def _gen_action_goto(self):
        """
        构造action goto表
        根据上述dfa生成Action Goto表格
        要考虑冲突的情况, 在action goto表中记录下来
        """
        # 深度优先遍历
        for node in self._for_each_dfa_node():
            items = node.conflict_detect(self.old_start)
            for item in items:
                if item[0].is_acc():  # 为了记录下一个状态，conflict_detect返回值是一个元组，记得吗
                    self.action_goto.set_item(node.id, self.bottom.name, item[0])
                elif item[0].is_reduce():
                    """这里应该将reduce项填入Follow(project.left)"""
                    to_add_token = list(self.follow[item[0].get_next_project().left.name])
                    to_add = [i.name for i in to_add_token]
                    self.action_goto.set_item_multi_cols(node.id, to_add, item[0])
                # 以下两种情况需要填入相应next_state再放入action goto表
                else:
                    next_token_name = item[1]  # 是一个name字符串
                    # 从out edge中寻找适合next_token的next_state
                    next_state = node.out_edge[next_token_name].id
                    item[0].set_next_state(next_state)
                    self.action_goto.set_item(node.id, next_token_name, item[0])

# =================================== for QT ===================================
    def get_action_goto_item(self):
        """为qt界面使用"""
        for i in range(self.action_goto.state_number):
            for j, key in enumerate(self.action_goto[i]):
                yield i, j, drop_color(str(self.action_goto[i][key]))

    def get_action_goto_state_num(self):
        """获得状态总数，为qt使用"""
        return self.action_goto.state_number

    def analyze_length(self):
        """返回分析的次数，为qt界面使用"""
        return len(self.a_state_stack)

# =================================== ====== ===================================

    def LR0(self, input_str=''):
        """
        由于单纯的LR(0)几乎没有实用性,这里直接支持SLR(1)了,但是函数名没有改
        可以版本回退查看单纯的LR(0)
        LR1和SLR1不同之处仅仅在于构造Action Goto表，有了ag表后这个函数可以重复使用
        :param input_str:
        :return:
        """
        def get_state_str():
            self.a_state_stack.append(str(state_stack))
            self.a_op_stack.append(str(op_stack))
            self.a_now_input_str.append(input_str[i:])
            self.a_action.append(action)
            return f'状态栈{str(state_stack): ^20} 符号栈{str(op_stack): ^25} 输入串 {input_str[i:]: ^15} 动作 {action: ^15}'
        op_stack = [self.bottom.name]
        state_stack = [0]
        i = 0  # 输入串
        action = ''  # 用于记录每次规约的动作
        while True:
            print(get_state_str())
            # 拿着当前栈顶状态和第一个输入串来查表
            state_top = state_stack[-1]
            input_token = input_str[i]  # 这里直接默认所有终结符和非终结符都是单字符了
            item = self.action_goto.get_item(state_top, input_token)
            if item.is_error_item():
                Zlog(f'规约时发生了错误[{get_state_str()}]')
                exit(-1)
            elif item.is_acc_item():
                break
            # 在构造SLR的ActionGoto时我发现, ActionGoto的表项不会出现冲突!
            elif not item.has_conflict():
                p_item = item[0]  # 没有冲突的情况，将唯一的元素取出来操作
                if p_item.is_shift() or p_item.is_goto():  # shift或goto都做移入操作
                    next_state = p_item.get_next_state()
                    state_stack.append(next_state)
                    op_stack.append(input_token)
                    i += 1
                    action = f'移入 {input_token}'
                else:  # 最复杂,需要规约的情况
                    next_project = p_item.get_next_project()
                    for token in reversed(next_project.right):
                        top = op_stack.pop()
                        state_stack.pop()
                        assert top == token.name
                    op_stack.append(next_project.left.name)  # 规约后的非终结符压栈
                    state_top = state_stack[-1]  # 通过现在的栈顶和非终结符查表
                    new_item = self.action_goto.get_item(state_top, op_stack[-1])

                    if not new_item.is_goto_item():  # 这里如果不是goto状态说明匹配失败
                        Zlog('不匹配')
                        exit(-1)
                    state_stack.append(new_item[0].get_next_state())  # 非终结符对应的状态也压栈
                    action = f'规约 {next_project}'
            # else:  # 有冲突的情况 TODO:这里肯定可以和上文没有冲突的情况进行合并
            #     # 根据follow集合选择究竟是shift还是reduce
            #     for p_item in item:
            #         input_token
        ZPass(msg='SLR(1)分析成功!')

    def show_projects(self):
        """just for input"""
        for proj in self.projects:
            print(proj)
        self.action_goto.show()
        self.show_ff()  # 需要用到随符集

    def show_dfa(self, filename):
        """
        使用 graphviz 可视化生成的DFA
        :param filename:
        :return:
        """
        g = graphviz.Digraph(filename=filename, engine='dot', node_attr={'shape': 'plaintext'})
        # g.attr('node', shape='rect')  # 这样得到的节点会有双框
        # 这里是不是又要深度搜索？脑子要炸了
        has_reached = []
        depth_stack = [self.dfa]
        while len(depth_stack) != 0:
            node = depth_stack.pop()  # 这里要确保栈中数据一定是没有遍历过的
            has_reached.append(node)
            g.node(str(node.id), str(node))
            for key in node.out_edge:  # 遍历每一个出边
                cur_node = node.out_edge[key]  # 判断下这个cur_node是否曾经入过栈
                if cur_node not in has_reached:
                    if cur_node != node:
                        g.node(str(cur_node.id), str(cur_node))  # 若不是重复节点，就创建新node
                        depth_stack.append(cur_node)
                    g.edge(str(node.id), str(cur_node.id), label=key, fontname="Microsoft YaHei")
                else:
                    g.edge(str(node.id), str(cur_node.id), label=key, fontname="Microsoft YaHei")
        g.render(format='svg')


def main():
    ebnf = SLRBNF('../../input/calc_2.txt')
    ebnf.show_ff()
    ebnf.show()
    ebnf.show_projects()
    ebnf.LR0('(i+i+i)*i#')


if __name__ == "__main__":
    main()

# 复杂程度有点超乎想象
# 逐渐失去掌控
# 写了很多除了现在的自己以外没人能看懂的代码
