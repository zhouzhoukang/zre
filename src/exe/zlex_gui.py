# -*- coding: utf-8 -*-
# FileName:     zlex_gui.py
# time:         22/11/8 008 上午 8:20
# Author:       Zhou Hang
# Description:  ZLex词法分析器GUI
import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QMessageBox

from src.core.Zlex import ZLexer
from src.ui.ZLex import Ui_ZLex


class ZLexGui(QMainWindow, Ui_ZLex):
    def __init__(self):
        super(ZLexGui, self).__init__()
        self.zlex = ZLexer()
        self.setupUi(self)
        self.setWindowIcon(QIcon('../../resource/pic/main_icon.png'))
        self.open_lex_btn.clicked.connect(self.open_lex_file)
        self.open_src_btn.clicked.connect(self.open_src_file)

    def open_filedialog(self, default_dir):
        lex_file_path, ok = QFileDialog.getOpenFileName(
            self, "选择lex文件", default_dir,
            "All Files (*);;Text Files (*.txt)"
        )
        if ok:
            return lex_file_path
        else:
            return None

    def open_lex_file(self):
        lex_file = self.open_filedialog("../../input/lexer")
        if lex_file is not None:
            ok = self.zlex.add_lex_file(lex_file)
            if ok is False:
                QMessageBox.critical(self, '错误', '不合法的lex文件\n请重新输入')
                self.lex_prompt.setText('重新输入词法文件❌')
            else:
                self.lex_prompt.setText('词法文件✅')

    def open_src_file(self):
        if self.zlex is None:
            QMessageBox.information(self, '提示', '请先输入lex文件')
            return
        src_file = self.open_filedialog("../../input/lexer")
        if src_file is not None:
            self.zlex.add_src_file(src_file)
            self.src_prompt.setText('源文件✅')
            self.zlex.run()

            # self.zlex.gen_beautiful_table()
            # self.output_view.append(str(self.zlex.res_table))
            # self.error_view.append(str(self.zlex.err_table))

            for line in self.zlex.results:
                self.output_view.append(str(line))

            for line in self.zlex.errors:
                self.error_view.append(str(line))


def main():
    app = QApplication(sys.argv)
    extra = {
        'font_size': '15px', 'QMenu': {
            'height': 50,
            'padding': '50px 50px 50px 50px',  # top, right, bottom, left
        }
    }
    # apply_stylesheet(app, theme='dark_amber.xml', extra=extra)
    view = ZLexGui()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
