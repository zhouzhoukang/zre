# -*- coding: utf-8 -*-
# FileName:     zparser_gui.py
# time:         22/11/10 010 上午 11:00
# Author:       Zhou Hang
# Description:  语法分析器的GUI
import sys
from pathlib import Path

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QIcon, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog, QTableWidgetItem, QMessageBox
from qtpy import QtWidgets

from src.core.SLR import SLRBNF
from src.core.UpDown import LL1
from src.core.OperatorFirst import OperatorFirst
from src.tools.visualize import svg2html
from src.ui.ZLL1 import Ui_ZLL1
from src.tools.misc import set2str
from src.tools.zerror import ZError
from src.tools.const import GrammarType


class ZLL1Gui(QWidget, Ui_ZLL1):
    """刚开始希望这个gui承载LL(1)文法的处理过程，后来发现其他分析法也可以借用这个界面"""

    def __init__(self):
        super(ZLL1Gui, self).__init__()
        self.setupUi(self)

        self.bnf = None
        self.to_deal_str = None
        self.bnf_file = None
        self.setWindowIcon(QIcon('../../resource/pic/main_icon.png'))
        knight = QPixmap('../../resource/pic/knight.png')
        self.knight_lbl.setScaledContents(True)
        self.knight_lbl.setPixmap(knight)

        self.open_bnf_btn.clicked.connect(self.open_bnf_file)
        self.sure_btn.clicked.connect(self.start_analyze)
        # 具体设置三个下拉框
        self.choose_grammar_type_cb.addItem(QIcon('../../resource/pic/down_up_arrow.png'), 'LL(1)')
        self.choose_grammar_type_cb.addItem(QIcon('../../resource/pic/up_down_arrow.png'), '算符优先')
        self.choose_grammar_type_cb.addItem(QIcon('../../resource/pic/up_down_arrow.png'), 'SLR(1)')
        # 设置TabWidget的图标
        self.update_tab_icons()
        self.grammar_type = GrammarType.LL1  # 默认
        self.choose_grammar_type_cb.currentTextChanged.connect(self.choose_grammar_type)

    def update_tab_icons(self):
        """为了xue微好看点"""
        self.show_tab.setTabIcon(0, QIcon('../../resource/pic/icecream.png'))
        self.show_tab.setTabIcon(1, QIcon('../../resource/pic/cake.png'))
        self.show_tab.setTabIcon(2, QIcon('../../resource/pic/egg.png'))
        self.show_tab.setTabIcon(3, QIcon('../../resource/pic/sandwich.png'))

    def choose_grammar_type(self):
        grammar = self.choose_grammar_type_cb.currentText()
        if grammar == 'LL(1)':
            self.grammar_type = GrammarType.LL1
            self.show_tab.setTabText(0, 'First/Follow')
            self.show_tab.setTabText(1, '预测分析表')
            self.show_tab.setTabText(3, '没有用')
        elif grammar == '算符优先':
            self.grammar_type = GrammarType.OP_FIRST
            self.show_tab.setTabText(0, 'Firstvt/Lastvt')
            self.show_tab.setTabText(1, '算符优先分析表')
            self.show_tab.setTabText(3, '不要点')
        elif grammar == 'SLR(1)':
            self.grammar_type = GrammarType.SLR1
            self.show_tab.setTabText(0, 'First/Follow')
            self.show_tab.setTabText(1, 'Action_Goto表')
            self.show_tab.setTabText(3, 'DFA')

    def open_filedialog(self, default_dir):
        lex_file_path, ok = QFileDialog.getOpenFileName(self, "选择lex文件", default_dir,
                                                        "All Files (*);;Text Files (*.txt)")
        if ok:
            return lex_file_path
        else:
            return None

    def open_bnf_file(self):
        bnf_file = self.open_filedialog("../../input/HW")
        if bnf_file is not None:
            # 根据用户的选择判断使用什么分析方法
            try:
                if self.grammar_type == GrammarType.LL1:
                    self.bnf = LL1(bnf_file)
                elif self.grammar_type == GrammarType.OP_FIRST:
                    self.bnf = OperatorFirst(bnf_file)
                elif self.grammar_type == GrammarType.SLR1:
                    self.bnf = SLRBNF(bnf_file)
            except ZError as e:
                QMessageBox.warning(self, '警告', f'{str(e)}')
                self.bnf_prompt.setText('请重新输入BNF文件❌')
                return
            self.bnf_prompt.setText('BNF文件✅')
            self.bnf_show.clear()
            for production in self.bnf.productions:
                self.bnf_show.append(str(production))
            # 开始生成First/Follow/预测分析表
            self.show_tables()
        else:
            self.bnf_show.setText('文件选择失败，请重新选择')

    def show_tables(self):
        """
        将first/follow/预测分析表
        或Firstvt/Lastvt/算符优先表
        结果填写到界面中
        """
        if self.grammar_type == GrammarType.LL1:
            self.LL1_show_tables()
        elif self.grammar_type == GrammarType.OP_FIRST:
            self.OP_FIRST_show_tables()
        elif self.grammar_type == GrammarType.SLR1:
            self.SLR1_show_tables()

    def show_ff_table(self):
        """供LL1文法和SLR文法使用，显示first/follow集合"""
        self.ff_table.setRowCount(self.bnf.get_non_terminal_num())
        self.ff_table.setColumnCount(3)
        self.ff_table.setHorizontalHeaderLabels(['非终结符', '首符集(FIRST)', '随符集(FOLLOW)'])
        self.ff_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for idx, key in enumerate(self.bnf.first):
            item = QTableWidgetItem(key)
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            self.ff_table.setItem(idx, 0, item)
            item = QTableWidgetItem(set2str(self.bnf.first[key]))
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            self.ff_table.setItem(idx, 1, item)
            item = QTableWidgetItem(set2str(self.bnf.follow[key]))
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            self.ff_table.setItem(idx, 2, item)

    def LL1_show_tables(self):
        # 生成FF表
        self.show_ff_table()
        # 生成预测分析表
        self.predict_table.setRowCount(self.bnf.get_non_terminal_num())
        self.predict_table.setColumnCount(self.bnf.get_terminal_num() + 1)
        self.predict_table.setHorizontalHeaderLabels([' '] + self.bnf.terminals)
        self.predict_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for row_id, non_terminal in enumerate(self.bnf.non_terminals):
            item = QTableWidgetItem(non_terminal)
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            self.predict_table.setItem(row_id, 0, item)
            for col_id, terminal in enumerate(self.bnf.terminals):
                try_item = self.bnf.prediction_table.get_item(non_terminal, terminal)
                if try_item == -1:
                    item = QTableWidgetItem('Error')
                    item.setForeground(QBrush(Qt.red))
                else:
                    item = QTableWidgetItem(str(try_item[0].get_one_str(try_item[1])))
                    item.setForeground(QBrush(Qt.darkGreen))
                self.predict_table.setItem(row_id, col_id + 1, item)

    def OP_FIRST_show_tables(self):
        def convert_vt_item_to_str(firstvt_item, lastvt_item):
            ret = '✓/' if firstvt_item else '/'
            ret += '⭐' if lastvt_item else ''
            return ret

        self.ff_table.clear()
        self.ff_table.setRowCount(self.bnf.get_non_terminal_num())
        self.ff_table.setColumnCount(self.bnf.get_terminal_num() + 1)
        self.ff_table.setHorizontalHeaderLabels([''] + self.bnf.terminals)
        self.ff_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        for col_id, terminal in enumerate(self.bnf.terminals):
            for row_id, non_terminal in enumerate(self.bnf.non_terminals):
                # 这里方便起见把firstvt和lastvt都放到同一张表格里了
                item = QTableWidgetItem(non_terminal)
                item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
                self.ff_table.setItem(row_id, 0, item)
                item = QTableWidgetItem(convert_vt_item_to_str(self.bnf.firstvt.get_item(non_terminal, terminal),
                                                               self.bnf.lastvt.get_item(non_terminal, terminal)))
                item.setForeground(QBrush(Qt.green))
                item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
                self.ff_table.setItem(row_id, col_id + 1, item)

        # 算符优先表显示
        self.predict_table.setRowCount(self.bnf.get_terminal_num() + 1)
        self.predict_table.setColumnCount(self.bnf.get_terminal_num() + 2)
        self.predict_table.setHorizontalHeaderLabels([''] + self.bnf.terminals + ['#'])
        for row_id, t1 in enumerate(self.bnf.terminals + ['#']):
            item = QTableWidgetItem(t1)
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            self.predict_table.setItem(row_id, 0, item)
            for col_id, t2 in enumerate(self.bnf.terminals + ['#']):
                priority = self.bnf.priority_table_get_str(t1, t2)
                item = QTableWidgetItem(priority)
                item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
                if priority == '≖':
                    item.setForeground(QBrush(Qt.darkYellow))
                elif priority == '⋖':
                    item.setForeground(QBrush(Qt.red))
                elif priority == '⋗':
                    item.setForeground(QBrush(Qt.darkGreen))
                self.predict_table.setItem(row_id, col_id + 1, item)

    def SLR1_show_tables(self):
        self.show_ff_table()
        # 显示action_goto表，这个表非常大，类名使用predict_table
        col_num = self.bnf.get_terminal_num() + 1 + self.bnf.get_non_terminal_num()
        self.predict_table.setColumnCount(col_num)
        self.predict_table.setRowCount(self.bnf.get_action_goto_state_num())
        self.predict_table.setHorizontalHeaderLabels(self.bnf.terminals + ['#'] + self.bnf.non_terminals)
        self.predict_table.setVerticalHeaderLabels([str(i) for i in range(self.bnf.get_action_goto_state_num())])
        for row_id, col_id, _str in self.bnf.get_action_goto_item():
            item = QTableWidgetItem(_str)
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            # 这里实际上是给原本带有ANSI Escape Code的字符串重新用qt风格来着色
            # 这里写的非常糟糕，但是没有必要改
            if _str[0] == 'E':
                item.setForeground(QBrush(Qt.red))
            elif _str[0] == 'S':
                item.setForeground(QBrush(Qt.darkYellow))
            elif _str[0] == 'A':
                item.setForeground(QBrush(Qt.darkGreen))
            elif len(_str) in [1, 2]:
                pass
            else:
                item.setForeground(QBrush(Qt.darkCyan))
            self.predict_table.setItem(row_id, col_id, item)

        # 生成DFA代码，添加到QWebEngineView中
        self.bnf.show_dfa("./tmp/dfa.gv")
        svg2html("./tmp/dfa.gv.svg", "./tmp/dfa.html")
        dfa_html = Path('./tmp/dfa.html')
        # print(dfa_html.read_text(encoding='utf-8'))
        self.dfa_web_show.setHtml(dfa_html.read_text(encoding='utf-8'))
        # self.dfa_web_show.setHtml("<html><body><p>hello world.</p></body></html>")

    def start_analyze(self):
        if self.grammar_type == GrammarType.LL1:
            self.start_LL1_analyze()
        elif self.grammar_type == GrammarType.OP_FIRST:
            self.start_OF_analyze()
        elif self.grammar_type == GrammarType.SLR1:
            self.start_SLR1_analyze()

    def start_LL1_analyze(self):
        to_deal_str = self.input_edit.text() + '#'
        try:
            self.bnf.recursive_descent_parsing(to_deal_str)
        except ZError as e:
            QMessageBox.warning(self, '错误', f'{str(e)}\n请重新输入符号串')
            self.input_edit.clear()
            return
        self.analyze_table.setRowCount(self.bnf.analyze_length())
        self.analyze_table.setColumnCount(3)
        self.analyze_table.setHorizontalHeaderLabels(['下推栈', '当前剩余字符串', '动作'])
        self.analyze_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for i in range(self.bnf.analyze_length()):
            item = QTableWidgetItem(self.bnf.a_pda_stack[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.red))
            self.analyze_table.setItem(i, 0, item)
            item = QTableWidgetItem(self.bnf.a_now_input_str[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkGreen))
            self.analyze_table.setItem(i, 1, item)
            item = QTableWidgetItem(self.bnf.a_action[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkYellow))
            self.analyze_table.setItem(i, 2, item)

    def start_OF_analyze(self):
        to_deal_str = self.input_edit.text() + '#'
        try:
            self.bnf.shift_reduce(to_deal_str)
        except ZError as e:
            QMessageBox.warning(self, '错误', f'{str(e)}\n请重新输入符号串')
            self.input_edit.clear()
            return
        self.analyze_table.setRowCount(self.bnf.analyze_length())
        self.analyze_table.setColumnCount(3)
        self.analyze_table.setHorizontalHeaderLabels(['分析栈', '当前剩余字符串', '动作'])
        self.analyze_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for i in range(self.bnf.analyze_length()):
            item = QTableWidgetItem(self.bnf.a_analyze_stack[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.red))
            self.analyze_table.setItem(i, 0, item)
            item = QTableWidgetItem(self.bnf.a_now_input_str[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkGreen))
            self.analyze_table.setItem(i, 1, item)
            item = QTableWidgetItem(self.bnf.a_action[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkYellow))
            self.analyze_table.setItem(i, 2, item)

    def start_SLR1_analyze(self):
        to_deal_str = self.input_edit.text() + '#'
        try:
            self.bnf.LR0(to_deal_str)
        except ZError as e:
            QMessageBox.warning(self, '错误', f'{str(e)}\n请重新输入符号串')
            self.input_edit.clear()
            return
        self.analyze_table.setRowCount(self.bnf.analyze_length())
        self.analyze_table.setColumnCount(4)
        self.analyze_table.setHorizontalHeaderLabels(['状态栈', '符号栈', '当前剩余字符串', '动作'])
        self.analyze_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for i in range(self.bnf.analyze_length()):
            item = QTableWidgetItem(self.bnf.a_state_stack[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.red))
            self.analyze_table.setItem(i, 0, item)
            item = QTableWidgetItem(self.bnf.a_op_stack[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkCyan))
            self.analyze_table.setItem(i, 1, item)
            item = QTableWidgetItem(self.bnf.a_now_input_str[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkGreen))
            self.analyze_table.setItem(i, 2, item)
            item = QTableWidgetItem(self.bnf.a_action[i])
            item.setTextAlignment(int(Qt.AlignHCenter) | int(Qt.AlignVCenter))
            item.setForeground(QBrush(Qt.darkYellow))
            self.analyze_table.setItem(i, 3, item)


def main():
    app = QApplication(sys.argv)
    # extra = {
    #     'font_size': '15px',
    #     'font_family': '得意黑',
    #     'QMenu': {
    #         'height': 50,
    #         'padding': '50px 50px 50px 50px',  # top, right, bottom, left
    #     }
    # }
    # 使用别人的库进行美化会导致某些地方无法自定义
    # apply_stylesheet(app, theme='dark_amber.xml', extra=extra)

    view = ZLL1Gui()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
