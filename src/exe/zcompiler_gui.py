# -*- coding: utf-8 -*-
# FileName:     zcompiler_gui.py.py
# time:         22/11/17 017 下午 4:08
# Author:       Zhou Hang
# Description:  I don't want to write
import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QApplication

from src.ui.ZCompiler import Ui_Choose_widget
from src.exe.zlex_gui import ZLexGui
from src.exe.zparser_gui import ZLL1Gui


class ChooseWidget(QWidget, Ui_Choose_widget):
    def __init__(self):
        super(ChooseWidget, self).__init__()
        self.zgrammar = None
        self.zlex = None
        self.setupUi(self)
        self.setWindowIcon(QIcon('../../resource/pic/main_icon.png'))
        self.open_lex_btn.clicked.connect(self.open_zlex)
        self.open_grammar_btn.clicked.connect(self.open_grammar)

    def open_zlex(self):
        self.zlex = ZLexGui()
        self.zlex.show()

    def open_grammar(self):
        self.zgrammar = ZLL1Gui()
        self.zgrammar.show()


def main():
    app = QApplication(sys.argv)
    extra = {'font_size': '15px', 'QMenu': {
        'height': 50,
        'padding': '50px 50px 50px 50px',  # top, right, bottom, left
    }}
    # apply_stylesheet(app, theme='dark_amber.xml', extra=extra)
    view = ChooseWidget()
    view.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
