# -*- coding: utf-8 -*-
# FileName:     OperatorFirst.py
# time:         22/9/27 025 下午 12:09
# Author:       Zhou Hang
# Description:  一些常量和Enum

from enum import Enum
# TODO: 此文件没理解原理,  研究如何在python中使用常量


class Const:
    MAX_RE_LEN = None
    REDUCE = None
    SHIFT = None
    EPSILON = None

    class ConstError(TypeError):
        pass

    class ContestCaseError(ConstError):
        pass

    def __setattr__(self, __name: str, __value: any) -> None:
        if self.__dict__.has_key(__name):
            raise self.ConstError(f'Can\'t change const.{__name}')
        if not __name.isupper():
            raise self.ConstCaseError(f'const name "{__name}" is not all uppercase')
        self.__dict__[__name] = __value

    def ConstCaseError(self, param):
        pass


Const.MAX_RE_LEN = 64
Const.EPSILON = 'ε'
Const.SHIFT = 0
Const.REDUCE = 1


class StateType(Enum):
    """
    将终结状态和其他状态分离，当然也可以只是用bool类型存放，但是为了扩展性
    """
    OTHER = 0  # 除了终结态的其他态
    END = 1  # 终结态


class Priority(Enum):
    """
    用于算符优先文法，表示两个符号的优先级
    """
    EQUAL = 0
    LOW = 1
    HIGH = 2


class GrammarType(Enum):
    """用于LL1_gui，区分不同的文法类型"""
    LL1 = 0
    OP_FIRST = 1
    SLR1 = 2
