# -*- coding: utf-8 -*-
# FileName:     zlog.py
# time:         22/10/6 006 下午 9:35
# Author:       Zhou Hang
# Description:  I don't want to write
import sys
import os
from src.tools.zerror import ZError


class Zlog:
    ANSI_BOLD = "\33[1;1m"
    ANSI_FG_BLACK = "\33[1;30m"
    ANSI_FG_RED = "\33[1;31m"
    ANSI_FG_GREEN = "\33[1;32m"
    ANSI_FG_YELLOW = "\33[1;33m"
    ANSI_FG_BLUE = "\33[1;34m"
    ANSI_FG_MAGENTA = "\33[1;35m"
    ANSI_FG_CYAN = "\33[1;36m"
    ANSI_FG_WHITE = "\33[1;37m"
    ANSI_BG_BLACK = "\33[1;40m"
    ANSI_BG_RED = "\33[1;41m"
    ANSI_BG_GREEN = "\33[1;42m"
    ANSI_BG_YELLOW = "\33[1;43m"
    ANSI_BG_BLUE = "\33[1;44m"
    ANSI_BG_MAGENTA = "\33[1;35m"
    ANSI_BG_CYAN = "\33[1;46m"
    ANSI_BG_WHITE = "\33[1;47m"
    ANSI_NONE = "\33[0m"

    def __init__(self, base_info, color='RED', if_print=True, show_info=True):
        if_raise = False
        if show_info:
            back_frame = sys._getframe().f_back
            back_filename = os.path.basename(back_frame.f_code.co_filename)
            # back_func_name = back_frame.f_code.co_name
            back_lineno = back_frame.f_lineno
            show_str = eval("self.ANSI_FG_" + color) + base_info + f' {back_filename}:{back_lineno}行' + self.ANSI_NONE
            self.show_str = show_str
            if_raise = True
        else:
            self.show_str = eval("self.ANSI_FG_" + color) + base_info + self.ANSI_NONE
        if if_print:
            print(self.show_str)
        if if_raise:
            raise ZError(base_info)

    def get_str(self):
        return self.show_str


def main():
    Zlog("input")


if __name__ == "__main__":
    main()
