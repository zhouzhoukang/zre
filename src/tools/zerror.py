# -*- coding: utf-8 -*-
# FileName:     zerror.py
# time:         22/11/15 015 上午 8:24
# Author:       Zhou Hang
# Description:  错误处理模块

class BaseError(Exception):
    def __str__(self):
        self.err_msg = ''
        self.err_msg_detail = ''


class ZError(BaseError):
    def __init__(self, err_msg):
        self.err_msg = {'code': 8000, 'message': '通用错误'}
        self.err_msg_detail = "通用错误" + err_msg
        Exception.__init__(self, self.err_msg, self.err_msg_detail)

# TODO 在这里添加更多 Exception


def ZAssert(cond: bool, msg: str) -> None:
    """
    do nothing if cond is true
    """
    if not cond:
        print(f"\33[1;31m{msg}\33[0m")
        exit(1)


def ZWarnning(cond: bool, msg: str) -> bool:
    if not cond:
        print(f"\33[1;33m{msg}\33[0m")
        return False
    else:
        return True


def ZPass(cond: bool = True, msg: str = "PASS"):
    if cond:
        print(f"\33[1;32m{msg}\33[0m")


def main():
    pass


if __name__ == "__main__":
    main()
