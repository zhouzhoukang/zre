# -*- coding: utf-8 -*-
# FileName:     misc.py
# time:         22/11/10 010 下午 11:20
# Author:       Zhou Hang
# Description:  I don't want to write

def set2str(_set):
    ret = ''
    for item in _set:
        ret += f'{str(item)}  '
    return ret.strip()


def drop_color(_str):
    """将字符串ANSI Escape Code删除"""
    if len(_str) < 11:
        return _str
    return _str[7: -4]


def lists_eq(l1, l2):
    """判断两个列表元素是否相同（顺序不一致无所谓）"""
    assert isinstance(l1, list)
    assert isinstance(l2, list)
    if len(l1) != len(l2):
        return False
    for i in l1:
        if i not in l2:
            return False
    return True


def main():
    _str = '\33[1;31mred\33[0m'
    print(drop_color(_str))
    l1 = [1, 2, 3]
    l2 = [2, 1, 4]
    print(lists_eq(l1, l2))


if __name__ == "__main__":
    main()
